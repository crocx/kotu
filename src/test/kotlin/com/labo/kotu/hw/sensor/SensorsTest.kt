package com.labo.kotu.hw.sensor

import com.labo.kotu.AppTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class SensorsTest : AppTest() {

    @Autowired
    lateinit var sensors: Sensors

    @Test
    fun getAllSensors() {
        log.info("Looking for all ${sensors.size} sensors:")
        checkSensorsList(sensors.getAllSensors().toList())
    }

    @Test
    fun getTemperatureSensors() {
        log.info("Looking for all TemperatureSensors in all ${sensors.size} sensors:")
        checkSensorsList(sensors.getSensorsByType<TemperatureSensor>())
    }

    @Test
    fun getMainSensors() {
        log.info("Looking for all ${sensors.size} main sensors:")
        checkSensorsList(sensors.mainSensors.values.toList())
    }

    private fun checkSensorsList(list: List<Sensor>) {
        assertThat(list).isNotEmpty
        list.forEach { log.info("Found sensor: $it") }
    }
}