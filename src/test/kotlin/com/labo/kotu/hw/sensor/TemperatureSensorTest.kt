package com.labo.kotu.hw.sensor

import com.labo.kotu.BaseTest
import com.labo.kotu.TestTag
import com.labo.kotu.hw.data.SensorErrorCode
import com.labo.kotu.hw.data.FloatSensorData
import com.labo.kotu.tool.Util
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo

internal class TemperatureSensorTest : BaseTest() {

    private val sensorId = TemperatureSensor.DEFAULT_SENSOR_ID
    private val tempSensor = TemperatureSensor(sensorId)
    private val tempSensorDataNotOk = FloatSensorData(sensorId, SensorErrorCode.CRC_FALSE)

    private lateinit var tempSensorData: FloatSensorData

    @BeforeEach
    override fun beforeEach(testInfo: TestInfo) {
        super.beforeEach(testInfo)
        tempSensor.oneWireFilepath = TemperatureSensor.TEST_FILE_OK
        tempSensorData = FloatSensorData(sensorId, TemperatureSensor.TEST_DEFAULT_TEMPERATURE)
    }

    @Test
    @Tag(TestTag.DONT_RUN_ON_PI)
    fun readTestData_testFileOK() {
        assertThat(tempSensor.readCurrentData()).isEqualToIgnoringGivenFields(tempSensorData, FloatSensorData::time.name)
    }

    @Test
    @Tag(TestTag.DONT_RUN_ON_PI)
    fun readTestData_testFileNotOK() {
        tempSensor.oneWireFilepath = TemperatureSensor.TEST_FILE_NOT_OK
        tempSensor.readCurrentData().let {
            assertThat(it).isEqualToIgnoringGivenFields(tempSensorDataNotOk, FloatSensorData::time.name, FloatSensorData::data.name)
            assertThat(it.data).isEqualTo(tempSensorDataNotOk.data) // NaN != NaN so this should do the trick
        }
    }

    @Test
    @Tag(TestTag.DONT_RUN_ON_PI)
    fun getCachedData_testFileOK() {
        tempSensor.getCachedData() // first get data to possibly read and then cache data
        tempSensorData.cached = true
        assertThat(tempSensor.getCachedData(1000)).isEqualToIgnoringGivenFields(tempSensorData, FloatSensorData::time.name)
    }

    @Test
    @Tag(TestTag.DONT_RUN_ON_PI)
    fun getCachedData_noOlderThan_testFileOK() {
        val noOlderThanTimeout = 100L
        val timeout = noOlderThanTimeout + 100L
        val time = Util.currentTime

        tempSensor.getCachedData() // first get data to possibly read and then cache data
        Thread.sleep(timeout) // sleep to make sure we get new data

        tempSensor.getCachedData(noOlderThanTimeout).let {
            assertThat(it).isEqualToIgnoringGivenFields(tempSensorData, FloatSensorData::time.name)
            assertThat(it.time.time).isGreaterThanOrEqualTo(time.time + timeout)
        }

        tempSensorData.cached = true
        assertThat(tempSensor.getCachedData(noOlderThanTimeout)).isEqualToIgnoringGivenFields(tempSensorData, FloatSensorData::time.name)
        tempSensor.getCachedData(noOlderThanTimeout).let {
            assertThat(it).isEqualToIgnoringGivenFields(tempSensorData, FloatSensorData::time.name)
            assertThat(it.time.time).isLessThanOrEqualTo(Util.currentTime.time)
        }
    }
}