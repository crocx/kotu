package com.labo.kotu.hw.service

import com.labo.kotu.TestTag
import com.labo.kotu.hw.data.SensorErrorCode
import com.labo.kotu.hw.data.FloatSensorData
import com.labo.kotu.hw.sensor.PollableSensor
import com.labo.kotu.hw.sensor.Sensors
import com.labo.kotu.hw.sensor.TemperatureSensor
import com.labo.kotu.main.service.BaseThreadServiceTest
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class SensorPollingServiceTest : BaseThreadServiceTest<Unit, SensorPollingService>() {

    data class TestSensorData<T>(val errorCode: SensorErrorCode, val data: T, val sensorId: String)

    private val pollTime = 1000L

    @Autowired
    private lateinit var sensors: Sensors

    private lateinit var sensor: PollableSensor<Float>
    private lateinit var testSensorData: TestSensorData<Float>

    override fun generateData() {
        // nothing to generate
    }

    override fun postConstruct() {
        sensor = sensors.mainSensors[TemperatureSensor::class] as TemperatureSensor
        testSensorData = TestSensorData(SensorErrorCode.OK, TemperatureSensor.TEST_DEFAULT_TEMPERATURE, sensor.sensorId)
        stopServiceBeforeStartingTests = true

        super.postConstruct()
    }

    @Test
    @Tag(TestTag.SLOWER)
    fun service_start_poll_stop_poll() {
        startServiceCheck(Unit)
        assertThat(service.startSensorPolling(sensor, pollTime)).isTrue()

        service.getData(sensor)?.let { assertThat(testSensorData).isEqualToComparingFieldByField(it) }
                ?: fail("Sensor data == null")

        Thread.sleep(pollTime)

        service.getData(sensor)?.let { assertThat(testSensorData).isEqualToComparingFieldByField(it) }
                ?: fail("Sensor data == null")

        // stop sensor polling should be successful the first time
        assertThat(service.stopSensorPolling(sensor)).isTrue()
        // stop sensor polling should fail the second time (already stopped)
        assertThat(service.stopSensorPolling(sensor)).isFalse()

        // shouldn't get sensor data once it's pooling has stopped
        assertThat(service.getData(sensor)).isNull()
        stopServiceCheck()
    }

    @Test
    fun getSensorById() {
        startServiceCheck(Unit)
        service.startSensorPolling(sensor, pollTime)
        assertThat(service.getSensorById<FloatSensorData>(sensor.sensorId)).isEqualToComparingFieldByField(sensor)
        service.stopSensorPolling(sensor) // cleanup
        stopServiceCheck()
    }
}