package com.labo.kotu.hw

import com.labo.kotu.AppTest
import com.labo.kotu.TestTag
import com.pi4j.io.gpio.PinState
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class PinControllerTest : AppTest() {

    private val pinNumber: Int = 1
    private val pinOffState = PinState.HIGH

    @Autowired
    private lateinit var pinController: PinController

    @BeforeEach
    @Tag(TestTag.DONT_RUN_ON_PI)
    fun init() {
        pinController.isInTestMode = true
    }

    @Test
    fun provisionPin() {
        assertThat(pinController.provisionPin(pinNumber, pinOffState, true)).isTrue()
    }

    @Test
    fun unprovisionPin() {
        assertThat(pinController.unprovisionPin()).isTrue()
    }

    @Test
    fun setPinOnOff() {
        /*
         * Has to be a state of:
         *   - off = LOW
         *   - on = HIGH
         */
        pinController.pinOffState = PinState.LOW
        assertThat(pinController.turnOn(true)).isEqualTo(PinState.HIGH)
        assertThat(pinController.turnOn(false)).isEqualTo(PinState.LOW)

        /*
         * Has to be a state of:
         *   - off = HIGH
         *   - on = LOW
         */
        pinController.pinOffState = PinState.HIGH
        assertThat(pinController.turnOn(true)).isEqualTo(PinState.LOW)
        assertThat(pinController.turnOn(false)).isEqualTo(PinState.HIGH)
    }
}