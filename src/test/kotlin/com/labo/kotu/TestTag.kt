package com.labo.kotu

object TestTag {

    /** Usually used for tests containing cumulative delays longer than 1 second. */
    const val SLOW = "slow"
    /** Usually used for tests containing cumulative delays up to 1 second. */
    const val SLOWER = "slower"

    const val DONT_RUN_ON_PI = "dontRunOnPi"

}
