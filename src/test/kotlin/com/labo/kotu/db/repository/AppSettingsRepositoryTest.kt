package com.labo.kotu.db.repository

import com.labo.kotu.AppTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class AppSettingsRepositoryTest : AppTest() {

    @Autowired
    private lateinit var appSettingsRepository: AppSettingsRepository

    @Test
    fun findFirst() {
        assertThat(appSettingsRepository.findFirstBy()).isNotNull()
    }

    @Test
    @Suppress("DEPRECATION")
    fun thereCanBeOnlyOne() {
        assertThat(appSettingsRepository.findAll()).hasSize(1)
    }

}