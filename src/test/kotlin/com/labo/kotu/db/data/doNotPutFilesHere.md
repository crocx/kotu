Don't put files in src/test/kotlin/com/labo/kotu/db/data/ because they get picked up by 
`factory().db().entityManager.registerEntityClasses("com.labo.kotu.db.data")` in `OrientDbDatabaseConfiguration`
(because of the same package name `com.labo.kotu.db.data`) and they get registered in the DB which is a no-no.
Instead, put them in a `com.labo.kotu.db.test.data` package.