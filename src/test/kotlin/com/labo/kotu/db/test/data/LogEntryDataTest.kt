package com.labo.kotu.db.test.data

import com.labo.kotu.AppTest
import com.labo.kotu.db.OrientDbDatabaseConfiguration
import com.labo.kotu.db.data.LogEntryData
import com.labo.kotu.db.repository.LogEntryRepository
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class LogEntryDataTest : AppTest() {

    @Autowired
    lateinit var dbConfig: OrientDbDatabaseConfiguration

    @Autowired
    lateinit var logEntryRepository: LogEntryRepository

    @BeforeEach
    @AfterEach
    fun init() {
        log.info("Removing all previous log entries from DB")
        logEntryRepository.deleteAll()
    }

    @Test
    fun dbInstance_isEqual() {
        log.info("Testing DB instance entry")
        val entry = dbConfig.factory().db().newInstance(LogEntryData::class.java)
        val savedEntry = equalsTagAndMessage(entry, "DB instance")

        Assertions.assertEquals(savedEntry, entry)
    }

    @Test
    fun plainInstance_isEqual() {
        log.info("Testing PLAIN instance entry")
        val entry = LogEntryData()
        val savedEntry = equalsTagAndMessage(entry, "PLAIN instance")

        Assertions.assertEquals(savedEntry, entry)
    }

    /**
     * This test doesn't work with @Transactional because in that case no commit is called and so no entries are
     * actually saved (that could be then counted)?
     */
    @Test
    fun allEntries_countMustBe2() {
        // add 2 instances
        addEntry(dbConfig.factory().db().newInstance(LogEntryData::class.java))
        addEntry(LogEntryData())
        val numOfInstances: Long = 2

        val count: Long = logEntryRepository.count()
        var i: Long = 0

        log.info("Found $count LogEntity entries")
        logEntryRepository.findAll().forEach {
            i++
            log.debug("Entry: $it")
        }

        Assertions.assertEquals(count, i)
        Assertions.assertEquals(numOfInstances, count)
    }

    fun equalsTagAndMessage(entry: LogEntryData, customMessage: String = "message"): LogEntryData {
        val tag = "tag-$currentDateTime"
        entry.tag = tag
        entry.message = customMessage

        val savedEntry = addEntry(entry)

        Assertions.assertEquals(savedEntry.tag, tag)
        Assertions.assertEquals(savedEntry.message, customMessage)
        return savedEntry
    }

    fun addEntry(entry: LogEntryData): LogEntryData {
        log.debug("Saving: entry = $entry")
        return logEntryRepository.save(entry)
    }

}