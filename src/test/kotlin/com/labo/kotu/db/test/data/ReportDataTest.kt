package com.labo.kotu.db.test.data

import com.labo.kotu.AppTest
import com.labo.kotu.db.data.BaseSensorDataCollection
import com.labo.kotu.db.data.ReportData
import com.labo.kotu.db.data.SensorDataBooleanCollection
import com.labo.kotu.db.data.SensorDataFloatCollection
import com.labo.kotu.db.repository.ReportsRepository
import com.labo.kotu.tool.Util
import com.labo.kotu.tool.addSeconds
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.util.*

internal class ReportDataTest : AppTest() {

    @Autowired
    lateinit var reportsRepository: ReportsRepository

    @Test
    fun removeAll() {
        reportsRepository.deleteAll()
    }

    @Test
    fun fillAndFindAllReports() {
        val report = ReportData(Util.currentTime, "Test notes")

        // add different sensor data types
        report.sensorsData.addAll(generateSensorsAndData(5, 10, { sensorId -> SensorDataFloatCollection(report, sensorId) }, { i -> 20 + 0.1f * i }))
        report.sensorsData.addAll(generateSensorsAndData(1, 15, { sensorId -> SensorDataBooleanCollection(report, sensorId) }, { i -> i % 2 == 0 }))

        log.info("Saving report")
        val savedReport = reportsRepository.save(report)

        log.info("Saved. Checking data for integrity")
        compareDbObjectsWithLists(report, savedReport)

        Assertions.assertThatThrownBy {
            log.info("Checking for expected error for corrupted data")
            report.sensorsData[0].setTime(0, Date(0))
            compareDbObjectsWithLists(report, savedReport)
        }

        log.info("Data checked")
    }

    /**
     * @param numSensors number of sensors to generate data for
     * @param numData number of entries per sensor to fill in the sensor's data array
     * @param factory a factory for generating SensorData of wanted type
     * @param generator a generator for generating SensorData typed data
     */
    private fun <T> generateSensorsAndData(
            numSensors: Int = 4,
            numData: Int = 10,
            factory: (sensorId: String) -> BaseSensorDataCollection<T>,
            generator: (i: Int) -> T
    ): Array<BaseSensorDataCollection<T>> {
        log.info("Generating $numData sensors data collections for $numSensors sensors; all data = ${numSensors * numData}")

        val list = arrayListOf<BaseSensorDataCollection<T>>()
        for (i in 0 until numSensors) {
            list.add(factory("Sensor ${i + 1}"))
        }

        val time = Util.currentTime
        for (i in 0 until numData) {
            val t = time.addSeconds(i)
            val data = generator(i)

            for (j in 0 until numSensors) {
                list[j].add(t, data)
            }
        }
        log.info("Data generated")

        return list.toTypedArray()
    }
}