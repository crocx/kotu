package com.labo.kotu.db.test.data

import com.labo.kotu.AppTest
import com.labo.kotu.main.AppSettings
import com.sun.corba.se.impl.activation.ServerMain.logError
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import kotlin.concurrent.thread

class AppSettingsDataTest : AppTest() {

    @Test
    fun loadAppSettings() {
        val settings = AppSettings.data
        log.info("Just loaded app setting: $settings")
        assertNotNull(settings, "AppSettings.data mustn't be null")
    }

    @Test
    fun saveAppSettings_newData() {
        log.info("Saving some data to app settings")
        val deviceId = currentDateTime

        AppSettings.data.deviceId = deviceId
        AppSettings.saveSettingsNow()

        assertThat(deviceId).isEqualTo(AppSettings.data.deviceId)

        // this won't work if this class or function is marked as @Transactional (no commit -> no version increase)
//        Assert.assertNotEquals("Version should increase after calling saveSettings()", AppSettings.data.version, appSettingsVersion)
    }

    @Test
    fun loadAppSettings_differentThread() {
        var exception: Exception? = Exception("The other thread wasn't run.")
        log.info("Test should now run on a new thread...")

        thread {
            log.info("Running on new thread: ${Thread.currentThread().name}")
            exception = try {
                log.info("Getting data from DB from a different thread than the DB was created on.")
                log.info("data: ${AppSettings.data}") // data has to be actually read
                log.info("Seems data was accessed successfully")
                null // return
            } catch (e: Exception) {
                logError(e.message!!)
                e // return
            }
            log.info("Ending the new thread.")

        }.join() // wait for this thread to finish

        log.info("Back to the original thread.")
        if (exception != null) throw exception as Exception
    }

    @Test
    fun saveAppSettings_differentThread() {
        var exception: Exception? = Exception("The other thread wasn't run.")
        log.info("Test should now run on a new thread...")

        thread {
            log.info("Running on new thread: ${Thread.currentThread().name}")
            exception = try {
                log.info("Saving data to DB from a different thread than the DB was created on.")
                AppSettings.saveSettingsNow()
                log.info("Seems data was saved successfully")
                null // return
            } catch (e: Exception) {
                logError(e.message!!)
                e // return
            }
            log.info("Ending the new thread.")

        }.join() // wait for this thread to finish

        log.info("Back to the original thread.")
        if (exception != null) throw exception as Exception
    }

//    @Test
//    fun testAddSensors() {
//        AppSettings.data.sensors.clear()
//        AppSettings.data.sensors.add(TemperatureSensor("foo"))
//        AppSettings.data.sensors.add(TemperatureSensor("bar"))
//    }
}