package com.labo.kotu.db.test.data

import com.labo.kotu.AppTest
import com.labo.kotu.db.data.ProgramData
import com.labo.kotu.db.repository.ProgramDataRepository
import com.labo.kotu.hw.sensor.Sensors
import com.labo.kotu.tool.Util
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class ProgramDataTest : AppTest() {

    @Autowired
    lateinit var programDataRepository: ProgramDataRepository
    @Autowired
    lateinit var sensors: Sensors

    @Test
    fun findAll() {
        generateSaveAndCheckProgramData() // ensure at least one entry

        log.info("Looking for all program data")
        val list = programDataRepository.findAll()
        assertThat(list).isNotEmpty
        log.info("Found ${list.size} entries")
        list.forEach { log.debug("\t$it") }
    }

    @Test
    fun saveProgram() {
        generateSaveAndCheckProgramData()
    }

    @Test
    fun removeGeneratedOne() {
        val savedData = generateSaveAndCheckProgramData()
        val savedId = savedData.id.toString()
        val gotData = programDataRepository.findById(savedId)
        log.info("Queried the same program to DB: $savedData")
        assertThat(gotData.isPresent).isTrue()
        assertThat(savedData).isEqualToIgnoringGivenFields(gotData.get(), "handler")

        log.info("Deleting saved program from DB: $savedId")
        programDataRepository.delete(savedData)
        val noData = programDataRepository.findById(savedId)
        assertThat(noData.isPresent).isFalse()
        log.info("All done")
    }

    private fun generateProgramData(): ProgramData {
        val data = ProgramData()

        data.setpoint = Util.nextDouble(upperLimit = 20.0)
        data.pidData.kP = Util.nextDouble()
        data.pidData.kI = Util.nextDouble()
        data.pidData.kD = Util.nextDouble()
        data.pidData.kF = Util.nextDouble()

        data.report.notes = "Test program started on ${Util.currentTime}"
        data.report.sensorsData = sensors.generateDataCollection(data.report)

        log.debug("Generated program data: $data")

        return data
    }

    fun generateSaveAndCheckProgramData(): ProgramData {
        val data = generateProgramData()
        val savedData = programDataRepository.save(data)

        log.info("Saved one program to DB: $savedData")

        assertThat(savedData.id).isNotNull
        assertThat(savedData.version).isNotNull
        compareDbObjects(savedData, data)

        return savedData
    }

}