package com.labo.kotu

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional

/**
 * Transactional by default, can use `@Transactional(propagation = Propagation.NEVER)` on a class/method that you want
 * exempt from being rolled-back at the end of the test.
 */
@Transactional
@SpringBootTest
abstract class AppTest : BaseTest()