package com.labo.kotu.web.controller

import com.labo.kotu.web.Routes
import com.labo.kotu.web.data.ProgramRestControllerWebData
import org.assertj.core.api.Assertions.assertThat
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MvcResult

@WebMvcTest(controllers = [ProgramRestController::class], secure = false)
class ProgramRestControllerTest : BaseRestControllerTest<ProgramRestControllerWebData>() {

    override val baseUrl = Routes.SERVICE_PROGRAM

    override fun generateData() = ProgramRestControllerWebData(22.222)

    override fun checkResponseParams(): (MvcResult) -> Unit {
        return {
            assertThat(mvcResultToObject<ProgramRestControllerWebData>(it)).isNotNull // redundant check given non-nullable type
        }
    }

    override fun preServiceUpdate() {
        data.setpoint = 33.333
    }
}