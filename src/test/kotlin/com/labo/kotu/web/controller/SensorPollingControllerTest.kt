package com.labo.kotu.web.controller

import com.labo.kotu.WebTest
import com.labo.kotu.hw.controller.SensorPollingController
import com.labo.kotu.hw.data.FloatSensorData
import com.labo.kotu.hw.sensor.Sensors
import com.labo.kotu.hw.sensor.TemperatureSensor
import com.labo.kotu.main.data.TemperatureControlData
import com.labo.kotu.main.service.BaseService
import com.labo.kotu.main.service.TemperatureControlService
import com.labo.kotu.web.Routes
import com.labo.kotu.web.data.SensorPollingWebData
import com.labo.kotu.web.response.FloatSensorResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import javax.annotation.PostConstruct

// TODO: this class should extend BaseRestControllerTest or similar
@WebMvcTest(controllers = [SensorPollingController::class], secure = false)
class SensorPollingControllerTest : WebTest() {

    @Autowired
    private lateinit var sensors: Sensors
    @Autowired
    private lateinit var service: TemperatureControlService

    private lateinit var testTemperatureData: FloatSensorData
    private lateinit var defaultSensorId: String
    private lateinit var data: TemperatureControlData
    private lateinit var response: FloatSensorResponse

    override val baseUrl = Routes.SERVICE_SENSOR

    @PostConstruct
    private fun postConstruct() {
        val sensor = sensors.getMainSensorByType<TemperatureSensor>()
        data = TemperatureControlData(sensor, TemperatureSensor.TEST_DEFAULT_TEMPERATURE.toDouble())
        defaultSensorId = sensor.sensorId
        testTemperatureData = FloatSensorData(defaultSensorId, TemperatureSensor.TEST_DEFAULT_TEMPERATURE)

        response = FloatSensorResponse(testTemperatureData)
        response.status = BaseService.Status.RUNNING
    }

    @BeforeAll
    private fun beforAll() {
        if (!service.isRunning()) assertThat(service.startService(data)).isTrue()
    }

    @AfterAll
    private fun afterAll() {
        assertThat(service.stopService()).isTrue()
    }

    @Test
    fun getSensorData_noSensorId() {
        val data = SensorPollingWebData()
        val mockUrl = MockMvcRequestBuilders.get("$baseUrl${BaseRestController.PATH_GET}")
        data.filterNonNullValues().forEach { p, v -> mockUrl.param(p, v) }

        mockMvc.perform(mockUrl)
                .andDo(printRequestUrl)
                .andExpect(expectOkResponse)
                .andDo(printResponseContent)
                .andDo {
                    mvcResultToObject<FloatSensorResponse>(it).let { result ->
                        assertThat(result).isEqualToIgnoringGivenFields(response, FloatSensorData::time.name)
                    }
                }
    }

    @Test
    fun getSensorData_wrongSensorId_error() {
        val data = SensorPollingWebData("wrongID")
        val mockUrl = MockMvcRequestBuilders.get("$baseUrl${BaseRestController.PATH_GET}")
        data.filterNonNullValues().forEach { p, v -> mockUrl.param(p, v) }

        mockMvc.perform(mockUrl)
                .andDo(printRequestUrl)
                .andDo(printShouldFail)
                .andExpect(MockMvcResultMatchers.status().is4xxClientError)
    }

}