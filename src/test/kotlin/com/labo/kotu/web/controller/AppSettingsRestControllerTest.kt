package com.labo.kotu.web.controller

import com.labo.kotu.WebTest
import com.labo.kotu.tool.build
import com.labo.kotu.web.Routes
import com.labo.kotu.web.data.AppSettingsWebData
import com.labo.kotu.web.response.AppSettingsServiceResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

@WebMvcTest(controllers = [AppSettingsRestController::class], secure = false)
class AppSettingsRestControllerTest : WebTest() {

    override val baseUrl = Routes.SERVICE_APP_SETTINGS

    @Test
    fun getStatusData() {
        mockMvc.perform(MockMvcRequestBuilders.get("$baseUrl${BaseRestController.PATH_STATUS_DATA}"))
                .andDo(printRequestUrl)
                .andExpect(expectOkResponse)
                .andDo(printResponseContent)
                .andDo {
                    mvcResultToObject<AppSettingsServiceResponse>(it).let { result ->
                        assertThat(result.data!!.paramsMap()).isEqualTo(AppSettingsWebData().build().paramsMap())
                    }
                }
    }

    @Test
    fun updateAllParams() {
        val data = AppSettingsWebData("new_sensor_id", 88, 88.2, 88.3, 88.4, 88.5, 888)
        val mockUrl = MockMvcRequestBuilders.get("$baseUrl${BaseRestController.PATH_UPDATE}")
        data.filterNonNullValues().forEach { param, value -> mockUrl.param(param, value) }

        mockMvc.perform(mockUrl)
                .andDo(printRequestUrl)
                .andExpect(expectOkResponse)
                .andDo(printResponseContent)
                .andDo {
                    mvcResultToObject<AppSettingsServiceResponse>(it).let { result ->
                        assertThat(result.data!!.paramsMap()).isEqualTo(data.paramsMap())
                    }
                }
    }
}