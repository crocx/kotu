package com.labo.kotu.web.controller

import com.labo.kotu.WebTest
import com.labo.kotu.main.service.BaseService
import com.labo.kotu.web.data.BaseWebData
import com.labo.kotu.web.response.BaseStatusDataResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import javax.annotation.PostConstruct

abstract class BaseRestControllerTest<T : BaseWebData> : WebTest() {

    val controllerGetUrl = BaseRestController.PATH_GET
    val controllerStartUrl = BaseRestController.PATH_START
    val controllerUpdateUrl = BaseRestController.PATH_UPDATE
    val controllerPauseUrl = BaseRestController.PATH_PAUSE
    val controllerResumeUrl = BaseRestController.PATH_RESUME
    val controllerStopUrl = BaseRestController.PATH_STOP
    val controllerStatusUrl = BaseRestController.PATH_STATUS

    protected abstract fun generateData(): T
    protected abstract fun checkResponseParams(): (MvcResult) -> Unit

    lateinit var data: T
    lateinit var paramsMap: MutableMap<String, String>

    @PostConstruct
    protected fun init() {
        data = generateData()
        updateParamsMap()
    }

    @Test
    fun start_pause_update_resume_stopService_allParams() {
        performUrlAndCheckServiceStatus(BaseService.Status.NOT_STARTED, BaseService.Status.ENDED)
        startService()
        performUrlAndCheckServiceStatus(BaseService.Status.RUNNING)
        pauseService()
        performUrlAndCheckServiceStatus(BaseService.Status.PAUSED)
        updateService()
        resumeService()
        performUrlAndCheckServiceStatus(BaseService.Status.RUNNING)
        stopService()
        performUrlAndCheckServiceStatus(BaseService.Status.ENDED)
    }

    @Test
    fun startService_noParams_error() {
        MockMvcRequestBuilders.get("$baseUrl$controllerStartUrl").let {
            mockMvc.perform(it)
                    .andDo(printRequestUrl)
                    .andDo(printShouldFail)
                    .andExpect(expectErrorMessage)
        }
    }

    @Test
    fun doubleStart_error() {
        log.info("Staring 1. service")
        startService()

        log.info("Staring 2. service")
        performUrlAndExpectError(controllerStartUrl)

        stopService() // clean-up after this test - don't leave any lingering started services
    }

    @Test
    fun updateService_notStarted_error() {
        performUrlAndExpectError(controllerUpdateUrl)
    }

    @Test
    fun stopService_notStarted_error() {
        performUrlAndExpectError(controllerStopUrl)
    }

    protected fun startService() {
        preServiceStart()
        performUrlAndCheckResponse(controllerStartUrl)
    }

    protected fun pauseService() {
        performUrlAndCheckResponse(controllerPauseUrl)
    }

    protected fun updateService() {
        preServiceUpdate()
        performUrlAndCheckResponse(controllerUpdateUrl)
    }

    protected fun resumeService() {
        performUrlAndCheckResponse(controllerResumeUrl)
    }

    protected fun stopService() {
        performUrlAndCheckResponse(controllerStopUrl)
    }

    private fun performUrlAndCheckResponse(url: String) {
        val mockUrl = MockMvcRequestBuilders.get("$baseUrl$url")
        fillMockUrlWithParams(mockUrl)

        mockMvc.perform(mockUrl)
                .andDo(printRequestUrl)
                .andExpect(expectOkResponse)
                .andDo(printResponseContent)
                .andExpect(checkResponseParams())
    }

    private fun performUrlAndExpectError(url: String) {
        val mockUrl = MockMvcRequestBuilders.get("$baseUrl$url")
        fillMockUrlWithParams(mockUrl)

        mockMvc.perform(mockUrl)
                .andDo(printRequestUrl)
                .andDo(printShouldFail)
                .andExpect(expectErrorResponse)
    }

    private fun performUrlAndCheckServiceStatus(serviceStatus: BaseService.Status, vararg additionalStatuses: BaseService.Status) {
        mockMvc.perform(MockMvcRequestBuilders.get("$baseUrl$controllerStatusUrl"))
                .andDo(printRequestUrl)
                .andExpect(expectOkResponse)
                .andDo { mvcResultToObject<BaseStatusDataResponse<*>>(it).let { response -> assertThat(response.status).isIn(serviceStatus, *additionalStatuses) } }
    }

    protected fun updateParamsMap() {
        paramsMap = data.filterNonNullValues().toMutableMap()
    }

    protected fun fillMockUrlWithParams(mockUrl: MockHttpServletRequestBuilder) {
        updateParamsMap()
        paramsMap.forEach { (param, value) -> mockUrl.param(param, value) }
    }

    /**
     * Overridable setup function
     */
    protected fun preServiceStart() {}

    /**
     * Overridable setup function
     */
    protected fun preServiceUpdate() {}
}