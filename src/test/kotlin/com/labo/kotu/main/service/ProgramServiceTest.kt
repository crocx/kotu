package com.labo.kotu.main.service

import com.labo.kotu.db.data.ProgramData
import com.labo.kotu.db.repository.ReportsRepository
import com.labo.kotu.main.AppSettings
import com.labo.kotu.tool.Util
import org.assertj.core.api.Assertions.assertThat
import org.springframework.beans.factory.annotation.Autowired

class ProgramServiceTest : BaseSuspendableServiceTest<ProgramData, ProgramService>() {

    @Autowired
    private lateinit var reportsRepository: ReportsRepository

    override fun generateData(): ProgramData {
        val data = ProgramData()
        data.pidData = AppSettings.data.pidData
        data.setpoint = 22.1
        data.report.notes = "Test report started on ${Util.currentTime}"
        return data
    }

    override fun startServiceCheck(data: ProgramData): ProgramData {
        assertThat(data.report.id).isNull()

        super.startServiceCheck(data)

        assertThat(reportsRepository.findById(service.data.id.toString())).isPresent
        assertThat(service.data.report.sensorsData).isNotEmpty
        compareDbObjects(service.data.pidData, AppSettings.data.pidData)

        return service.data
    }

}
