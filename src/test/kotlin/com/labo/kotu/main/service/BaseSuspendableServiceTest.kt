package com.labo.kotu.main.service

import com.labo.kotu.TestTag
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.opentest4j.AssertionFailedError

abstract class BaseSuspendableServiceTest<T : Any, S : BaseSuspendableService<T>> : BaseServiceTest<T, S>() {

    fun pauseServiceCheck() {
        assertThat(service.isRunning()).isTrue()
        assertThat(service.pauseService()).isTrue()
        assertThat(service.isPaused()).isTrue()
    }

    fun resumeServiceCheck() {
        assertThat(service.isPaused()).isTrue()
        assertThat(service.resumeService()).isTrue()
        assertThat(service.isRunning()).isTrue()
    }

    @Test
    fun start_pause_resume_stop() {
        startServiceCheck(data)
        pauseServiceCheck()
        resumeServiceCheck()
        stopServiceCheck()
    }

    @Test
    @Tag(TestTag.SLOW)
    fun startFor3Sec_pauseFor3Sec_resumeFor3Sec_stop() {
        startServiceCheck(data)
        Thread.sleep(3000)
        pauseServiceCheck()
        Thread.sleep(3000)
        resumeServiceCheck()
        Thread.sleep(3000)
        stopServiceCheck()
    }

    @Test
    fun notStarted_pause_warning() {
        assertThrows<AssertionFailedError> { pauseServiceCheck() }
    }

    @Test
    fun notStarted_resume_warning() {
        assertThrows<AssertionFailedError> { resumeServiceCheck() }
    }

}