package com.labo.kotu.main.service

import com.labo.kotu.AppTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import javax.annotation.PostConstruct

abstract class BaseServiceTest<T : Any, S : BaseService<T>> : AppTest() {

    abstract fun generateData(): T

    protected lateinit var data: T

    @Autowired
    protected lateinit var service: S

    /** Also starts it back up after all the tests have been run. */
    protected var stopServiceBeforeStartingTests = false

    private lateinit var restoreData: T

    @PostConstruct
    fun postConstruct() {
        data = generateData()
    }

    @BeforeEach
    fun testStart() {
        if (stopServiceBeforeStartingTests) {
            restoreData = service.data
            assertThat(service.stopService()).isTrue()
        }
        if (service.isRunning()) log.warn("## Service ${service.className} was running before the start of the test. Is that OK?")

        log.debug("## Starting test for service = $service")
    }

    @AfterEach
    fun testEnded() {
        log.debug("## Ended test for service = $service")
        if (stopServiceBeforeStartingTests) assertThat(service.startService(restoreData)).isTrue()
    }

    fun startServiceCheck(data: T): T {
        assertThat(service.status).isIn(BaseService.Status.NOT_STARTED, BaseService.Status.ENDED)
        assertThat(service.startService(data)).isTrue()
        assertThat(service.isRunning()).isTrue()
        return data
    }

    fun stopServiceCheck() {
        assertThat(service.isRunning()).isTrue()
        assertThat(service.stopService()).isTrue()
        assertThat(service.isStopped()).isTrue()
    }

    @Test
    fun doubleStart_warning() {
        startServiceCheck(data)
        assertThat(service.startService(data)).isFalse()
        stopServiceCheck() // cleanup
    }

    @Test
    fun doubleStart2_warning() {
        startServiceCheck(data)
        assertThrows<AssertionError> { startServiceCheck(data) }
        stopServiceCheck() // cleanup
    }

    @Test
    fun notStarted_stop_warning() {
        assertThrows<AssertionError> { stopServiceCheck() }
    }

}