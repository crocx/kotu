package com.labo.kotu.main.service

import com.labo.kotu.TestTag
import com.labo.kotu.db.data.AppSettingsData
import com.labo.kotu.db.data.PidData
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

class AppSettingsServiceTest : BaseServiceTest<AppSettingsData, AppSettingsService>() {

    override var data: AppSettingsData
        get() = service.data
        set(_) {} // don't set anything

    override fun generateData() = data

    override fun postConstruct() {
        stopServiceBeforeStartingTests = true

        super.postConstruct()
    }

    /**
     * Disable transactions here because saving data in a new thread (when delayed) executes in a different transaction
     * (ie. not in this test transaction), so that (new/changed) data gets saved while this "restoreData" gets rolled
     * back (doesn't get saved - because it uses saveSettingsNow() which executes in this test transaction (that gets
     * rolled back)).
     *
     * XXX: Could be fixed with coroutines?
     */
    @Transactional(propagation = Propagation.NEVER)
    @Test
    @Tag(TestTag.SLOW)
    fun saveDataDelayed() {
        val restoreData = AppSettingsData(data)

        assertThat(service.data).isEqualTo(data)

        data.gpioPinNumber = 99
        data.refreshTime = 999
        data.diffTolerance = 99.1
        data.pidData = PidData(99.2, 99.3, 99.4, 99.5)
        data.deviceId = "test id"
        data.sensorId = "test sensor"

        assertThat(service.updateService()).isTrue()
        // wait for save timeout to pass to test delayed saving settings
        Thread.sleep(service.saveTimeout + 100)
        assertThat(service.data).isEqualTo(data)

        service.copyData(restoreData)
        compareDbObjects(service.data, restoreData)
    }
}