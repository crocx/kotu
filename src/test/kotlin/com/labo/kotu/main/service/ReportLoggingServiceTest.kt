package com.labo.kotu.main.service

import com.labo.kotu.db.data.ReportData
import com.labo.kotu.hw.sensor.Sensors
import org.springframework.beans.factory.annotation.Autowired

internal class ReportLoggingServiceTest : BaseThreadServiceTest<ReportData, ReportLoggingService>() {

    @Autowired
    private lateinit var sensors: Sensors

    override fun generateData(): ReportData {
        val reportData = ReportData(notes = "Test report data from ReportLoggingServiceTest")
        reportData.sensorsData = sensors.generateDataCollection(reportData)
        reportData.loggingInterval = 1000
        return reportData
    }

}