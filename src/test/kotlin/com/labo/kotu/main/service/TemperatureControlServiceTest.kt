package com.labo.kotu.main.service

import com.labo.kotu.TestTag
import com.labo.kotu.hw.data.FloatSensorData
import com.labo.kotu.hw.data.SensorErrorCode
import com.labo.kotu.hw.sensor.Sensors
import com.labo.kotu.hw.sensor.TemperatureSensor
import com.labo.kotu.main.data.TemperatureControlData
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class TemperatureControlServiceTest : BaseThreadServiceTest<TemperatureControlData, TemperatureControlService>() {

    @Autowired
    override lateinit var service: TemperatureControlService
    @Autowired
    private lateinit var sensors: Sensors

    private lateinit var sensor: TemperatureSensor
    private lateinit var testTemperatureData: FloatSensorData

    override fun postConstruct() {
        sensor = sensors.mainSensors[TemperatureSensor::class] as TemperatureSensor
        testTemperatureData = FloatSensorData(sensor.sensorId, TemperatureSensor.TEST_DEFAULT_TEMPERATURE)

        super.postConstruct()
    }

    override fun generateData(): TemperatureControlData {
        return TemperatureControlData(sensor, TemperatureSensor.TEST_DEFAULT_TEMPERATURE.toDouble())
    }

    @Test
    @Tag(TestTag.DONT_RUN_ON_PI)
    fun getTemperature_cached() {
        service.getTemperature(sensor) // get first data that might not be cached
        val data = service.getTemperature(sensor)

        assertThat(data.data).isEqualTo(testTemperatureData.data)
        assertThat(data.sensorId).isEqualTo(testTemperatureData.sensorId)
        assertThat(data.cached).isTrue()
        assertThat(data.errorCode).isEqualTo(SensorErrorCode.OK)
    }

}