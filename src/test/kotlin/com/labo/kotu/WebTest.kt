package com.labo.kotu

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.labo.kotu.web.response.BaseResponse
import com.labo.kotu.web.response.BaseResponseCodes
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultHandler
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.transaction.annotation.Transactional

/**
 * Transactional by default, can use `@Transactional(propagation = Propagation.NEVER)` on a class/method that you want
 * exempt from being rolled-back at the end of the test.
 */
@Transactional
@WebMvcTest
@ComponentScan("com.labo.kotu")
abstract class WebTest : BaseTest() {

    abstract val baseUrl: String

    @Autowired
    protected lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper
        protected set

    private val statusMatcher = MockMvcResultMatchers.status()

    final inline fun <reified T : Any> mvcResultToObject(mvcResult: MvcResult, printObject: Boolean = true): T {
        val obj = objectMapper.readValue<T>(mvcResult.response.contentAsString)
        if (printObject) log.debug("mvcResultToObject: object = $obj")
        return obj
    }

    val printRequestUrl = ResultHandler { log.info("requestURL: ${it.request.requestURL}") }

    val printResponseContent = ResultHandler { log.info("Response: ${it.response.contentAsString}") }

    val printShouldFail = ResultHandler { log.info("Should fail: code = ${it.response.status}, message = ${it.response.errorMessage}") }

    val expectOkResponse: ResultMatcher = ResultMatcher {
        statusMatcher.isOk.match(it)
        mvcResultToObject<BaseResponse>(it).let { response -> assertThat(response.errorCode).isEqualTo(BaseResponseCodes.OK) }
    }

    /**
     * Expect error response when there is a service error
     */
    val expectErrorResponse: ResultMatcher = ResultMatcher {
        statusMatcher.is4xxClientError.match(it)
        mvcResultToObject<BaseResponse>(it).let { response -> assertThat(response.errorCode).isNotEqualTo(BaseResponseCodes.OK) }
    }

    /**
     * Expect error message when there is a missing URL param error for instance (not a service response error)
     */
    val expectErrorMessage: ResultMatcher = ResultMatcher {
        statusMatcher.is4xxClientError.match(it)
        assertThat(it.response.errorMessage).isNotEmpty()
    }


    /**
     * Just base path (eg. '/api/v1/temp') should return an error.
     */
    @Test
    fun basePath_error4xx() {
        mockMvc.perform(MockMvcRequestBuilders.get(baseUrl))
                .andDo(printRequestUrl)
                .andDo(printShouldFail)
                .andExpect(statusMatcher.is4xxClientError)
    }

}