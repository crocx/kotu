package com.labo.kotu

import com.labo.kotu.tool.LLog
import com.labo.kotu.tool.Util
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInfo
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class BaseTest {

    protected val log = LLog

    protected val currentDateTime get() = Util.currentTime.toString()

    protected val listClass = List::class.java
    protected val ignoreDbMetaFields = arrayOf("id", "version", "handler")
    protected val listComparator = Comparator<List<*>> { o1, o2 ->
        if (accessFieldsBeforeEvaluation) {
            o1.toString()
            o2.toString()
        }
        //        logDebug("comparing fields:\no1 = $o1\no2 = $o2")
        Assertions.assertThat(o1).usingElementComparatorIgnoringFields(*ignoreDbMetaFields).isEqualTo(o2)
        0
    }

    /**
     * This is sometimes needed in order to get the DB to fetch fields we're about to read so that we can compare
     * relevant data (and not relevant data to possible null data).
     */
    protected var accessFieldsBeforeEvaluation = false

    @BeforeEach
    open fun beforeEach(testInfo: TestInfo) {
        log.debug("### TEST - START: ${testInfo.displayName} ###")
    }

    @AfterEach
    fun afterEach(testInfo: TestInfo) {
        log.debug("### TEST - END: ${testInfo.displayName} ###")
    }

    /**
     * Used for comparing objects where at least one of these objects has been loaded from DB (and may not have all their
     * fields initialised).
     */
    fun compareDbObjectsWithLists(o1: Any, o2: Any, accessFieldsBeforeEvaluation: Boolean = true) {
        val orig = this.accessFieldsBeforeEvaluation
        this.accessFieldsBeforeEvaluation = accessFieldsBeforeEvaluation
        Assertions.assertThat(o1).usingComparatorForType(listComparator, listClass).isEqualToIgnoringGivenFields(o2, *ignoreDbMetaFields)
        this.accessFieldsBeforeEvaluation = orig
    }

    /**
     * Compares two objects by fields ignoring DB meta fields listed in [ignoreDbMetaFields]
     */
    fun compareDbObjects(o1: Any, o2: Any) {
        Assertions.assertThat(o1).isEqualToIgnoringGivenFields(o2, *ignoreDbMetaFields)
    }
}