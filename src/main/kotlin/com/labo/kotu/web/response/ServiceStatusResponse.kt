package com.labo.kotu.web.response

import com.labo.kotu.main.service.BaseService

interface ServiceStatusResponse {

    var status: BaseService.Status

}
