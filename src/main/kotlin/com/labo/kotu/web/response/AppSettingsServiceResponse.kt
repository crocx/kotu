package com.labo.kotu.web.response

import com.labo.kotu.web.data.AppSettingsWebData

class AppSettingsServiceResponse(data: AppSettingsWebData? = null) : BaseStatusDataResponse<AppSettingsWebData>(data)