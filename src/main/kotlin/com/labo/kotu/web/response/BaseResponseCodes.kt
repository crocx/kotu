package com.labo.kotu.web.response

enum class BaseResponseCodes(val code: Int) {
    OK(0),

    /*
     * General errors 1 - 99
     */

    ERROR(1),

    /*
     * Service errors 100 - 999
     */

    ERROR_SERVICE_ALREADY_STARTED(110),
    ERROR_SERVICE_NOT_STARTED(120),
    ERROR_SERVICE_NOT_PAUSED(130),
    ERROR_SERVICE_NOT_UPDATED(140),
    ERROR_SERVICE_NOT_RESUMED(150),
    ERROR_SERVICE_NOT_STOPPED(160),
    ERROR_SERVICE_NOT_RUNNING(160),

    ERROR_CANNOT_POLL_SENSOR(510),

    /*
     * Hardware errors 1000 - 1999
     */

    ERROR_HW_PIN_PROVISIONING_FAILED(1110),

    ERROR_NO_SENSOR_WITH_ID(1210),

    ;

    fun formatedCode() = "${this.name} (${this.code})"
}