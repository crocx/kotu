package com.labo.kotu.web.response

import com.labo.kotu.main.service.BaseService
import com.labo.kotu.web.data.ProgramRestControllerWebData

class ProgramServiceResponse(data: ProgramRestControllerWebData? = null, status: BaseService.Status = BaseService.Status.NOT_STARTED) : BaseStatusDataResponse<ProgramRestControllerWebData>(data, status)
