package com.labo.kotu.web.response

import com.labo.kotu.tool.Util
import java.util.*

open class BaseResponse(var errorCode: BaseResponseCodes = BaseResponseCodes.OK,
                        var message: String? = null,
                        var time: Date = Util.currentTime) {

    override fun toString(): String {
        return "BaseResponse(errorCode=$errorCode, message=$message, time=$time)"
    }
}
