package com.labo.kotu.web.response

import com.labo.kotu.hw.data.SensorData

open class SensorPollingServiceResponse<T : SensorData<*>>(data: T? = null) : BaseStatusDataResponse<T>(data)