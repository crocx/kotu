package com.labo.kotu.web.response

import com.labo.kotu.hw.data.FloatSensorData

class FloatSensorResponse(data: FloatSensorData) : SensorPollingServiceResponse<FloatSensorData>(data)