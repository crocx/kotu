package com.labo.kotu.web.response

import com.labo.kotu.main.service.BaseService

open class BaseStatusDataResponse<T>(override val data: T? = null, override var status: BaseService.Status = BaseService.Status.NOT_STARTED) : BaseResponse(), DataResponse<T>, ServiceStatusResponse