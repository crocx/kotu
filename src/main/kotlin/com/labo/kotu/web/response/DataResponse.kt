package com.labo.kotu.web.response

interface DataResponse<T> {

    val data: T?

}