package com.labo.kotu.web

object Routes {

    const val CORS_ORIGIN_ALL = "*"

    const val PATH_API = "/api"
    const val PATH_VERSION_1 = "/v1"
    const val PATH_CURRENT_VERSION = PATH_VERSION_1
    const val PATH_CURRENT = "$PATH_API$PATH_CURRENT_VERSION"

    const val SERVICE_PROGRAM = "$PATH_CURRENT/program"
    const val SERVICE_SENSOR = "$PATH_CURRENT/sensor"
    const val SERVICE_APP_SETTINGS = "$PATH_CURRENT/appsettings"
}
