package com.labo.kotu.web.controller

import com.labo.kotu.hw.data.FloatSensorData
import com.labo.kotu.hw.sensor.Sensors
import com.labo.kotu.hw.sensor.TemperatureSensor
import com.labo.kotu.hw.service.SensorPollingService
import com.labo.kotu.tool.isFalse
import com.labo.kotu.web.Routes
import com.labo.kotu.web.response.BaseResponseCodes
import com.labo.kotu.web.response.FloatSensorResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Routes.SERVICE_SENSOR)
@CrossOrigin(Routes.CORS_ORIGIN_ALL)
class SensorPollingRestController : BaseRestController<SensorPollingService>() {

    @Autowired
    override lateinit var service: SensorPollingService
    @Autowired
    private lateinit var sensors: Sensors

    /** path: /get?id={id} */
    @GetMapping(PATH_GET)
    fun getTemperature(@RequestParam(required = false) sensorId: String?)
            : ResponseEntity<FloatSensorResponse> {

        service.isRunning().isFalse {
            log.error("Service ${service.className} not started")
            return responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_RUNNING)
        }

        var sensor = if (!sensorId.isNullOrBlank()) {
            service.getSensorById<TemperatureSensor>(sensorId!!) as? TemperatureSensor
                    ?: return responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_NO_SENSOR_WITH_ID)
        } else {
            sensors.getMainSensorByType<TemperatureSensor>()
//                    .also { log.debug("No sensorId specified, returning main sensor by type: $it") }
        }

        return responseEntityWithServiceStatus(FloatSensorResponse(service.getData(sensor) as FloatSensorData), service)
    }

}
