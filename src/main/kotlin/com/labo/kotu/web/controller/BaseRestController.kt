package com.labo.kotu.web.controller

import com.labo.kotu.main.service.BaseService
import com.labo.kotu.tool.LLog
import com.labo.kotu.web.Routes
import com.labo.kotu.web.response.BaseResponse
import com.labo.kotu.web.response.BaseResponseCodes
import com.labo.kotu.web.response.BaseStatusDataResponse
import com.labo.kotu.web.response.ServiceStatusResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping

@CrossOrigin(Routes.CORS_ORIGIN_ALL)
abstract class BaseRestController<S : BaseService<*>> {

    companion object {
        const val PATH_GET = "/get"
        const val PATH_START = "/start"
        const val PATH_PAUSE = "/pause"
        const val PATH_UPDATE = "/update"
        const val PATH_RESUME = "/resume"
        const val PATH_STOP = "/stop"
        const val PATH_STATUS = "/status"

        const val PATH_STATUS_DATA = "$PATH_STATUS/data"
    }

    protected abstract var service: S

    protected val log = LLog

    protected fun <T : BaseResponse> responseEntity(response: T): ResponseEntity<T> = ResponseEntity(response, toHttpStatus(response.errorCode))

    protected fun <T> responseEntityWithServiceStatus(response: T, service: BaseService<*>): ResponseEntity<T> where T : BaseResponse, T : ServiceStatusResponse {
        return ResponseEntity(response, toHttpStatus(response.errorCode)).also { response.status = service.status }
    }

    protected fun <T : BaseResponse> responseEntityWithErrorOnFail(responseCode: BaseResponseCodes, response: T? = null, failed: Boolean = true): ResponseEntity<T> {
        if (response == null) return ResponseEntity(BaseResponse(responseCode) as T, toHttpStatus(responseCode))
        if (failed) response.errorCode = responseCode
        return ResponseEntity(response, toHttpStatus(response.errorCode))
    }

    private fun toHttpStatus(responseCode: BaseResponseCodes): HttpStatus = if (responseCode == BaseResponseCodes.OK) HttpStatus.OK else HttpStatus.BAD_REQUEST

    @GetMapping(PATH_STATUS)
    fun getServiceStatus() = BaseStatusDataResponse(null, service.status)

}