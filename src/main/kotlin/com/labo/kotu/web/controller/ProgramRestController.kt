package com.labo.kotu.web.controller

import com.labo.kotu.db.data.ProgramData
import com.labo.kotu.main.service.ProgramService
import com.labo.kotu.tool.builder
import com.labo.kotu.tool.isFalse
import com.labo.kotu.web.Routes
import com.labo.kotu.web.data.ProgramRestControllerWebData
import com.labo.kotu.web.response.BaseResponseCodes
import com.labo.kotu.web.response.ProgramServiceResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.SERVICE_PROGRAM)
class ProgramRestController : BaseRestController<ProgramService>() {

    @Autowired
    override lateinit var service: ProgramService

    @GetMapping(PATH_START)
    @Synchronized
    fun startService(@RequestParam setpoint: Double): ResponseEntity<ProgramServiceResponse> {
        if (service.isRunningOrPaused()) return responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_ALREADY_STARTED)

        return service.startService(ProgramData().builder(setpoint)).let { started -> responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_STARTED, defaultResponse(), !started) }
    }

    @GetMapping(PATH_PAUSE)
    fun pauseService(): ResponseEntity<ProgramServiceResponse> {
        return service.pauseService().let { paused -> responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_PAUSED, defaultResponse(), !paused) }
    }

    @GetMapping(PATH_UPDATE)
    fun updateService(@RequestParam(required = false) setpoint: Double?): ResponseEntity<ProgramServiceResponse> {
        if (!service.isRunningOrPaused()) return responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_RUNNING, defaultResponse(), true)

        setpoint?.let { service.data.setpoint = it }

//        return service.updateService().let { updated -> responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_UPDATED, defaultResponse(), !updated) }
        service.updateService().isFalse { return responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_UPDATED) }

        val response = ProgramServiceResponse(ProgramRestControllerWebData(), service.status)
        response.data?.let { data ->
            // set/send back only those params that came in (as a conformation that of their new value)
            setpoint?.let { data.setpoint = it }
        }

        return responseEntity(response)
    }

    @GetMapping(PATH_RESUME)
    fun resumeService(): ResponseEntity<ProgramServiceResponse> {
        return service.resumeService().let { resumed -> responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_RESUMED, defaultResponse(), !resumed) }
    }

    @GetMapping(PATH_STOP)
    fun stopService(): ResponseEntity<ProgramServiceResponse> {
        return service.stopService().let { stopped -> responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_STOPPED, defaultResponse(), !stopped) }
    }

    private fun defaultResponse() = ProgramServiceResponse(status = service.status)
}
