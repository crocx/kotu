package com.labo.kotu.web.controller

import com.labo.kotu.main.service.AppSettingsService
import com.labo.kotu.tool.build
import com.labo.kotu.tool.isFalse
import com.labo.kotu.web.Routes
import com.labo.kotu.web.data.AppSettingsWebData
import com.labo.kotu.web.response.AppSettingsServiceResponse
import com.labo.kotu.web.response.BaseResponseCodes
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.SERVICE_APP_SETTINGS)
class AppSettingsRestController : BaseRestController<AppSettingsService>() {

    @Autowired
    override lateinit var service: AppSettingsService

    /** path: /status/data */
    @GetMapping(PATH_STATUS_DATA)
    fun getStatusData(): ResponseEntity<AppSettingsServiceResponse> {
        service.isRunning().isFalse { return responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_RUNNING) }

        return responseEntity(AppSettingsServiceResponse(AppSettingsWebData().build()))
    }

    /** path: /update */
    @GetMapping(PATH_UPDATE)
    fun updateService(
            @RequestParam(required = false) sensorId: String?,
            @RequestParam(required = false) kP: Double?,
            @RequestParam(required = false) kI: Double?,
            @RequestParam(required = false) kD: Double?,
            @RequestParam(required = false) gpio: Int?,
            @RequestParam(required = false) diffTolerance: Double?,
            @RequestParam(required = false) refreshTime: Long?
    ): ResponseEntity<AppSettingsServiceResponse> {

        service.isRunning().isFalse { return responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_RUNNING) }

        sensorId?.let { service.data.sensorId = it }
        kP?.let { service.data.pidData.kP = it }
        kI?.let { service.data.pidData.kI = it }
        kD?.let { service.data.pidData.kD = it }
        gpio?.let { service.data.gpioPinNumber = it }
        diffTolerance?.let { service.data.diffTolerance = it }
        refreshTime?.let { service.data.refreshTime = it }

        service.updateService().isFalse { return responseEntityWithErrorOnFail(BaseResponseCodes.ERROR_SERVICE_NOT_UPDATED) }

        // only set/return the params that were sent in URL
        val data = AppSettingsWebData()
        sensorId?.let { data.sensorId = service.data.sensorId }
        kP?.let { data.kP = service.data.pidData.kP }
        kI?.let { data.kI = service.data.pidData.kI }
        kD?.let { data.kD = service.data.pidData.kD }
        gpio?.let { data.gpio = service.data.gpioPinNumber }
        diffTolerance?.let { data.diffTolerance = service.data.diffTolerance }
        refreshTime?.let { data.refreshTime = service.data.refreshTime }

        return responseEntity(AppSettingsServiceResponse(data))
    }


}