package com.labo.kotu.web.controller

import com.labo.kotu.tool.LLog
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
//@ControllerAdvice
class ErrorHandlingController {

    private val log = LLog

    /**
     * Redirect URLs like "/temperature" to "home page" URL.
     */
    @RequestMapping("/{path:\\w+}")
    fun redirectToHome(request: HttpServletRequest, response: HttpServletResponse) {
        log.debug("Request: $request")

        val redirectToHome = "/" // or "/index.html" but that can show in the URL of the browser which isn't that nice
        response.sendRedirect(redirectToHome)

        log.debug("Response: $response, status: ${response.status}")
    }

//    @ExceptionHandler(Exception::class)
//    fun handleConflict(ex: Exception, request: WebRequest): ResponseEntity<Any> {
//        log.debug("Redirecting request to: ${Routes.SERVICE_SENSOR}")
//        return ResponseEntity(HttpStatus.TEMPORARY_REDIRECT)
//    }
}
