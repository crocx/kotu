package com.labo.kotu.web.data

class ProgramRestControllerWebData : BaseWebData {

    var setpoint: Double? by ParamDelegate()

    constructor(setpoint: Double? = null) : super() {
        this.setpoint = setpoint
    }
}
