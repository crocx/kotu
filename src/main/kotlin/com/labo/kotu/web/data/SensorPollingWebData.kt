package com.labo.kotu.web.data

class SensorPollingWebData : BaseWebData {

    var sensorId: String? by ParamDelegate()

    constructor(sensorId: String? = null) : super() {
        this.sensorId = sensorId
    }
}