package com.labo.kotu.web.data

class AppSettingsWebData : BaseWebData {

    var sensorId: String? by ParamDelegate()
    var gpio: Int? by ParamDelegate()

    var kP: Double? by ParamDelegate()
    var kI: Double? by ParamDelegate()
    var kD: Double? by ParamDelegate()

    var diffTolerance: Double? by ParamDelegate()
    var refreshTime: Long? by ParamDelegate()

    @Suppress("ConvertSecondaryConstructorToPrimary") // no benefit in converting
    constructor(sensorId: String? = null, gpio: Int? = null, kP: Double? = null, kI: Double? = null, kD: Double? = null, diffTolerance: Double? = null, refreshTime: Long? = null) {
        // must initialise non-nullable properties
        this.sensorId = sensorId
        this.gpio = gpio
        this.kP = kP
        this.kI = kI
        this.kD = kD
        this.diffTolerance = diffTolerance
        this.refreshTime = refreshTime
    }

    override fun toString(): String {
        val sb = StringBuilder()
        paramToValueMap.forEach { key, value -> sb.append(", $key=$value") }
        return "AppSettingsWebData(${sb.removePrefix(", ")})" // offset of 2 is to remove the first ", "
    }

}
