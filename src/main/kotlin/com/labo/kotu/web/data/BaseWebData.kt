package com.labo.kotu.web.data

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

abstract class BaseWebData {

    class ParamDelegate<T : Any?>(defaultValue: T? = null, private val overrideParamName: String? = null) : ReadWriteProperty<BaseWebData, T> {

        private var value: T = defaultValue as T

        override fun getValue(thisRef: BaseWebData, property: KProperty<*>): T {
//            log.debug("Getting value '$value' for property: $property, thisRef = $thisRef")
            return value
        }

        override fun setValue(thisRef: BaseWebData, property: KProperty<*>, value: T) {
//            log.debug("Setting value '$value' for property: $property, thisRef = $thisRef")
            this.value = value
            thisRef.paramToValueMap[overrideParamName ?: property.name] = value?.toString()
        }
    }

    protected val paramToValueMap = mutableMapOf<String, String?>()

    /**
     * Creates a new instance of immutable map of [paramToValueMap].
     */
    // return copy; don't want outside manipulation of this map
    fun paramsMap(): Map<String, String?> = paramToValueMap.toMap()

    operator fun get(param: String): String? = paramToValueMap[param]

    /**
     * Return a map of non-null param-value pairs.
     */
    fun filterNonNullValues(): Map<String, String> = paramToValueMap.filterValues { it != null }.mapValues { it.value!! }

}
