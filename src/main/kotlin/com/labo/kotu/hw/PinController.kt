package com.labo.kotu.hw

import com.labo.kotu.tool.LLog
import com.pi4j.io.gpio.GpioPinDigitalOutput
import com.pi4j.io.gpio.PinState
import com.pi4j.io.gpio.RaspiPin
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.annotation.PreDestroy

@Component
class PinController {

    private val log = LLog

    var pinOffState: PinState = PinState.LOW
    var isInTestMode: Boolean = false
    var pinName: String? = null

    @Autowired
    private lateinit var gpio: GPIO

    private var pin: GpioPinDigitalOutput? = null

    fun provisionPin(pinNumber: Int, pinOffState: PinState = this.pinOffState, testMode: Boolean? = null): Boolean {
        log.debug("Provisioning pin: pinNumber = $pinNumber, pinOffState = $pinOffState, testMode = $testMode")
        this.pinOffState = pinOffState

        if (testMode != null) isInTestMode = testMode
        if (isInTestMode) return true
        if (pin != null) return false

        pin = gpio.controller.provisionDigitalOutputPin(RaspiPin.getPinByAddress(pinNumber), pinName, pinOffState)
        pin!!.setShutdownOptions(true, pinOffState)

        log.debug("Provisioned pin: $pin, is exported = ${gpio.controller.isExported(pin)}")
        return true
    }

    fun unprovisionPin(): Boolean {
        log.debug("Unprovisioning pin: $pin")

        if (isInTestMode) return true
        pin ?: return false

        turnOn(false) // turn pin off
        gpio.controller.unprovisionPin(pin)
        log.debug("Unprovisioned pin: $pin")
        pin = null

        return true
    }

    fun turnOn(on: Boolean): PinState? {
        if (isInTestMode) {
            return if ((pinOffState == PinState.LOW) == on) PinState.HIGH else PinState.LOW
        }
        pin ?: return null

        pin?.setState((pinOffState == PinState.LOW) == on)
        log.debug("Turn pin ON = $on, pin state = ${pin?.state}")
        return pin?.state
    }

    @PreDestroy
    private fun onDestroy() {
        log.debug("onDestroy: Unprovisioning pin: $pin")
        unprovisionPin()
    }

}