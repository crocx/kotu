package com.labo.kotu.hw.sensor

abstract class Sensor {

    abstract val sensorId: String

    open lateinit var sensorName: String

    override fun toString(): String {
        return "[sensorId = '$sensorId', sensorName = '$sensorName']"
    }

}