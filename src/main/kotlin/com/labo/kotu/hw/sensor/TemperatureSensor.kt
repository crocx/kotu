package com.labo.kotu.hw.sensor

import com.labo.kotu.hw.data.SensorErrorCode
import com.labo.kotu.hw.data.FloatSensorData
import com.labo.kotu.tool.LLog
import com.labo.kotu.tool.Util
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.stream.Collectors
import java.util.stream.Stream

class TemperatureSensor(override val sensorId: String) : PollableSensor<Float>() {

    companion object {
        const val DEFAULT_SENSOR_ID = "28-0516a4efbeff"
        const val TEST_FILE_OK = "src/main/resources/testData/temperatureOK.txt"
        const val TEST_FILE_NOT_OK = "src/main/resources/testData/temperatureNotOK.txt"
        const val TEST_DEFAULT_TEMPERATURE: Float = 20.001f
    }

    var oneWireFilepath: String = TEST_FILE_NOT_OK
    var isGettingTestData = false

    // TODO: remove this now that we have SensorPolingService?
    private lateinit var cachedSensorData: FloatSensorData

    private val log = LLog

    init {
        sensorName = "Temp 1-wire"
        oneWireFilepath = "/sys/bus/w1/devices/$sensorId/w1_slave"

        if (!Files.exists(Paths.get(oneWireFilepath))) {
            oneWireFilepath = TEST_FILE_OK
            isGettingTestData = true
        }
    }

    override fun readSensor(): FloatSensorData {
        return readCurrentData()
    }

    private fun getTemperatureData(): FloatSensorData {
        val sensorData: String
        val returnData = FloatSensorData(sensorId)

        lateinit var file: Stream<String>
        try {
            file = Files.lines(Paths.get(oneWireFilepath))
            sensorData = file.collect(Collectors.joining("\n"))
        } catch (e: IOException) {
            returnData.errorCode = SensorErrorCode.CANNOT_READ_SENSOR_VALUE
            log.warn("Can't read 1-wire file, returning error code: ${returnData.errorCode}, exception: ${e.message}")
            return returnData
        } catch (e1: Exception) {
            returnData.errorCode = SensorErrorCode.GENERAL_EXCEPTION
            log.error("Some general exception, returning error code: ${returnData.errorCode}, exception: ${e1.message}")
            return returnData
        } finally {
            file.close()
        }

        log.trace("temperature data:\n$sensorData")

        if (!sensorData.contains("YES")) {
            returnData.errorCode = SensorErrorCode.CRC_FALSE
            log.warn("CRC != YES, returning error code: ${returnData.errorCode}")
            return returnData
        }

        val temperature = sensorData.substring(sensorData.indexOf("t=") + 2).trim()
        returnData.data = temperature.toFloat() / 1000
        returnData.time = Util.currentTime // because some time has probably passed since the creation of this object

        return returnData
    }

    /**
     * Will read current data but that might be slow (up to 1000 ms or more). Use [getCachedData] to get cached data
     * instantly (you also get the time the data was cached so you know how old it is).
     */
    fun readCurrentData(): FloatSensorData {
        val returnData = getTemperatureData()
        cachedSensorData = returnData
        return returnData
    }

    /**
     * Get cached data instantly instead of waiting for it by calling [readCurrentData].
     *
     * @param noOlderThanMs limits how old the cached data can be since it was last updated. Value of 2000 ms for example
     * would mean that this cached data is no more than that many milliseconds old.
     */
    fun getCachedData(noOlderThanMs: Long? = null): FloatSensorData {
        var isCached = true
        if (!::cachedSensorData.isInitialized || noOlderThanMs != null && (cachedSensorData.time.time + noOlderThanMs) < Util.currentTime.time) {
            readCurrentData()
            isCached = false
        }

        /*
         If 'cachedSensorData.cached == true' it means it still references 'returnData' from the last 'readCurrentData()'
         call. We want to return a copy of it if one hasn't been made already and set 'cached = true'.
        */
        if (!cachedSensorData.cached) {
            cachedSensorData = cachedSensorData.copy()
            cachedSensorData.cached = isCached
        }

        return cachedSensorData
    }

    override fun toString(): String {
        return "TemperatureSensor(${super.toString()}, oneWireFilepath='$oneWireFilepath', isGettingTestData=$isGettingTestData)"
    }

}
