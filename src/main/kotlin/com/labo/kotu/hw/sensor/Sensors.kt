package com.labo.kotu.hw.sensor

import com.labo.kotu.db.data.BaseSensorDataCollection
import com.labo.kotu.db.data.ReportData
import com.labo.kotu.db.data.SensorDataFloatCollection
import com.labo.kotu.tool.LLog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import java.nio.file.Files
import java.nio.file.Paths
import javax.annotation.PostConstruct
import kotlin.reflect.KClass

@Configuration
class Sensors {

    @Configuration
    @PropertySource("classpath:sensors.properties")
    @ConfigurationProperties(prefix = "sensors")
    class ConfigProperties {

        enum class SensorType {
            TEMPERATURE, HUMIDITY
        }

        var autoDetectSensors: Boolean = true
        var useFakeSensors = false
        var useFakeSensorsIfAutoDetectFails = false
        var fakeSensors = hashMapOf<String, SensorType>()

        override fun toString(): String {
            return "ConfigProperties(autoDetectSensors=$autoDetectSensors, useFakeSensors=$useFakeSensors, useFakeSensorsIfAutoDetectFails=$useFakeSensorsIfAutoDetectFails, fakeSensors=$fakeSensors)"
        }
    }

    val PATH_TO_1_WIRE_SENSORS = "/sys/bus/w1/devices"
    val PATH_TO_1_WIRE_SENSORS_SUFFIX = "w1_slave"
    val log = LLog

    @Autowired
    lateinit var config: ConfigProperties

    val size get() = sensors.size

    // XXX: what to do in case of multiple work places where every work place has it's own main (temperature) sensor?
    val mainSensors = hashMapOf<KClass<out Sensor>, Sensor>()

    private val sensors = hashMapOf<String, Sensor>()

    operator fun get(sensorId: String) = sensors[sensorId]

    fun getAllSensors() = sensors.values

    @PostConstruct
    private fun init() {
        log.debug("Sensors config: $config")
        var autoDetectSuccessful = false

        // DEBUG: used for testing
        if (config.autoDetectSensors) {
            val path = Paths.get(PATH_TO_1_WIRE_SENSORS)
            log.info("Auto detecting sensors. Looking in: $path")

            if (!Files.exists(path)) {
                log.warn("Auto detecting sensors failed. No such path found: $path")
            } else {
                for (dir in Files.list(path)) {
                    val sensorId = dir.fileName.toString()
                    log.debug("\tFound dir: $dir, fileName: $sensorId")

                    if (sensorId.startsWith("28-")) { // add 1-wire temperature sensors
                        val temperatureSensor = TemperatureSensor(sensorId)
                        sensors[sensorId] = temperatureSensor
                        addFirstMainSensor(temperatureSensor)
                        log.info("\t- Added temperature sensor: $temperatureSensor")
                    }
                }
                autoDetectSuccessful = true
            }
        }

        if (config.useFakeSensors || (config.useFakeSensorsIfAutoDetectFails && !autoDetectSuccessful)) {
            log.info("Adding fake sensors.")
            for ((sensorId, type) in config.fakeSensors) {
                when (type) {
                    ConfigProperties.SensorType.TEMPERATURE -> {
                        val s = TemperatureSensor(sensorId)
                        sensors[sensorId] = s
                        addFirstMainSensor(s)
                    }

                    else -> log.warn("Fake sensor with id '$sensorId' is of unknown type: $type")
                }
            }

            log.debug(sensors.toString())
        }

        log.debug("Sensors: $sensors")
    }

    final inline fun <reified T : Sensor> getSensorsByType(): List<T> {
        val list = arrayListOf<T>()
        getAllSensors().forEach { sensor -> if (sensor is T) list.add(sensor) }
        return list
    }

    final inline fun <reified T : Sensor> getMainSensorByType(): T {
        return mainSensors[T::class] as? T
                ?: throw RuntimeException("No main sensor found for type: ${T::class.simpleName}")
    }

    fun generateDataCollection(report: ReportData): ArrayList<BaseSensorDataCollection<*>> {
        val list = arrayListOf<BaseSensorDataCollection<*>>()
        sensors.forEach { sensorId, _ -> list.add(SensorDataFloatCollection(report, sensorId)) }
        return list
    }

    /**
     * The first added sensor of a given Sensor class is the main sensor for that class/type by default.
     */
    private fun addFirstMainSensor(sensor: Sensor) {
        if (mainSensors[sensor::class] == null) mainSensors[sensor::class] = sensor
    }
}