package com.labo.kotu.hw.sensor

import com.labo.kotu.hw.data.SensorData

abstract class PollableSensor<T> : Sensor() {

    abstract fun readSensor(): SensorData<T>

}