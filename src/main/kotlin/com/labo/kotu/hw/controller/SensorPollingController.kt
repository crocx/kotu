package com.labo.kotu.hw.controller

import com.labo.kotu.hw.data.SensorData
import com.labo.kotu.hw.sensor.PollableSensor
import com.labo.kotu.hw.sensor.Sensors
import com.labo.kotu.hw.service.SensorPollingService
import com.labo.kotu.tool.LLog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import javax.annotation.PostConstruct

@Controller
class SensorPollingController {

    val defaultPollingTime = 1000L // in ms

    private val log = LLog

    @Autowired
    private lateinit var pollingService: SensorPollingService
    @Autowired
    private lateinit var sensors: Sensors

    @PostConstruct
    private fun init() {
        // TODO: instantiating some main (eg. temperature) sensor polling should be elsewhere
//        pollingService.startSensorPolling(TemperatureSensor(TemperatureSensor.DEFAULT_SENSOR_ID), defaultPollingTime)
//        pollingService.startSensorPolling(TemperatureSensor("temp_1"), defaultPollingTime)
        // XXX: start polling all pollable sensors on start by default
        sensors.getSensorsByType<PollableSensor<*>>().forEach { pollingService.startSensorPolling(it, defaultPollingTime) }
    }

    fun <T> getSensorData(sensorId: String, cached: Boolean = true): SensorData<T>? {
        pollingService.getSensorById<T>(sensorId)?.let { return getSensorData(it, cached) }

        log.warn("Can't get data for sensorId = $sensorId, cached = $cached")
        return null
    }

    fun <T> getSensorData(sensor: PollableSensor<T>, cached: Boolean = true): SensorData<T>? {
        if (cached) return pollingService.getData(sensor)

        log.debug("Reading sensor for non-cached data: $sensor")
        return sensor.readSensor()
    }

    fun <T> startSensorPolling(sensor: PollableSensor<T>): Boolean = pollingService.startSensorPolling(sensor, defaultPollingTime)

    fun <T> isAlreadyPollingSensor(sensor: PollableSensor<T>): Boolean = pollingService.getSensorById<T>(sensor.sensorId) != null
}
