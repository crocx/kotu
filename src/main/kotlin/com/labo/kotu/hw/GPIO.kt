package com.labo.kotu.hw

import com.labo.kotu.tool.LLog
import com.pi4j.io.gpio.GpioController
import com.pi4j.io.gpio.GpioFactory
import com.pi4j.wiringpi.GpioUtil
import org.springframework.stereotype.Component
import javax.annotation.PreDestroy

@Component
class GPIO {

    private val log = LLog

    private val gpioLazy = lazy {
        log.info("Creating GPIO controller.")
        log.info("GPIO: isPrivilegedAccessRequired = ${GpioUtil.isPrivilegedAccessRequired()}")
        GpioUtil.enableNonPrivilegedAccess()
        GpioFactory.getInstance() // return
    }

    val controller: GpioController by gpioLazy

    val isGpioReady get() = gpioLazy.isInitialized()

    @PreDestroy
    private fun onDestroy() {
        if (isGpioReady) {
            log.info("Shutting down GPIO controller.")
            controller.shutdown()
        }
    }

}