package com.labo.kotu.hw.data

enum class SensorErrorCode {
    OK,
    GENERAL_EXCEPTION,
    CANNOT_READ_SENSOR_VALUE,
    CRC_FALSE,
    SENSOR_NOT_POLLING,
}
