package com.labo.kotu.hw.data

import com.labo.kotu.main.annotation.AllOpen
import com.labo.kotu.tool.Util
import java.util.*

@AllOpen
abstract class SensorData<T>(var sensorId: String,
                             var time: Date = Util.currentTime,
                             var cached: Boolean = false,
                             var errorCode: SensorErrorCode = SensorErrorCode.OK) {

    abstract var data: T

    override fun toString(): String {
        return "SensorData(sensorId='$sensorId', time=$time, cached=$cached, errorCode=$errorCode)"
    }
}
