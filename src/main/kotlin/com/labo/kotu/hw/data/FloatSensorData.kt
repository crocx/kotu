package com.labo.kotu.hw.data

data class FloatSensorData(
        override var sensorId: String,
        override var data: Float = Float.NaN)
    : SensorData<Float>(sensorId) {

    constructor(sensorId: String, errorCode: SensorErrorCode) : this(sensorId, Float.NaN) {
        this.errorCode = errorCode
    }

    override fun toString(): String {
        return "FloatSensorData(data=$data, ${super.toString()})"
    }

}