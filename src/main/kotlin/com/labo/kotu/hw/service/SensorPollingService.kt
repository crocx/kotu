package com.labo.kotu.hw.service

import com.labo.kotu.hw.data.SensorData
import com.labo.kotu.hw.sensor.PollableSensor
import com.labo.kotu.main.service.BaseThreadService
import org.springframework.stereotype.Service
import javax.annotation.PreDestroy
import kotlin.concurrent.thread

@Service
class SensorPollingService : BaseThreadService<Unit>() {

    override var tagServiceThread = "SPS"

    inner class PollItem<T : SensorData<*>>(val thread: Thread, var data: T)

    private val sensorMap = mutableMapOf<PollableSensor<*>, PollItem<SensorData<*>>>()

    override fun inServiceLoop(): Long {
        // simple loop, sleep for 10 sec
        return 10000
    }

    override fun onServiceLoopPause() {
        // pause polling sensors "if (wantedStatus == PAUSED)"
        sensorMap.values.forEach { it.thread.interrupt() }
    }

    override fun onServiceLoopResume() {
        // resume polling sensors "if (wantedStatus == RUNNING)"
        sensorMap.values.forEach { it.thread.interrupt() }
    }

    override fun onServiceLoopEnd() {
        sensorMap.values.forEach {
            log.debug("Stopping sensor thread: ${it.thread.name}", tagServiceThread)
            it.thread.interrupt()
            it.thread.join()
        }
    }

    /**
     * @param sensor
     * @param pollTime how often should the sensor be polled in ms (ignoring sensor response time)
     */
    fun <T> startSensorPolling(sensor: PollableSensor<T>, pollTime: Long): Boolean {
        if (sensorMap[sensor] != null) return true

        log.info("Starting polling thread for sensor: $sensor")
        val sensorData = sensor.readSensor()
        sensorData.cached = true

        val thread = thread(name = sensor.sensorName) {
            var timeStart: Long
            var timeEnd: Long
            log.info("Starting polling loop: pollTime = $pollTime ms, sensor: $sensor")

            while (wantToRun()) {
                try {
                    timeStart = System.currentTimeMillis()
                    val data = sensor.readSensor()
                    data.cached = true
                    timeEnd = System.currentTimeMillis()
                    sensorMap[sensor]!!.data = data

                    // sleep at most for 'pollTime' ms
                    val timeout = pollTime - (timeEnd - timeStart)
                    if (timeout > 0) {
//                    log.debug("Sleeping for: $timeout, data = $data")
                        Thread.sleep(timeout)
                    }
                } catch (e: InterruptedException) {
                    if (!checkAndPauseLoop()) break
                }
            }

            sensorMap.remove(sensor)
            log.info("Polling loop ended for sensor: $sensor")
        }

        sensorMap.put(sensor, PollItem(thread, sensorData))

        return true
    }

    fun <T> getData(sensor: PollableSensor<T>): SensorData<T>? {
        return sensorMap[sensor]?.data as? SensorData<T>
    }

    fun <T> stopSensorPolling(sensor: PollableSensor<T>): Boolean {
        val item = sensorMap[sensor] ?: return false

        log.info("Stopping polling thread for sensor: $sensor")
        item.thread.interrupt()
        item.thread.join()
        return true
    }

    fun <T> getSensorById(id: String): PollableSensor<T>? {
        return sensorMap.keys.find { it.sensorId == id } as? PollableSensor<T>
    }

    @PreDestroy
    fun onDestroy() {
        sensorMap.values.forEach {
            it.thread.interrupt()
            it.thread.join()
        }
    }
}
