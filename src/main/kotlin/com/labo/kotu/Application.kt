package com.labo.kotu

import com.labo.kotu.db.data.AppSettingsData
import com.labo.kotu.hw.service.SensorPollingService
import com.labo.kotu.main.service.AppSettingsService
import com.labo.kotu.tool.LLog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

val log = LLog

fun main(args: Array<String>) {
//    val logger = LoggerFactory.getLogger(Application::class.java)

    log.debug("Starting Spring app...")

    SpringApplication.run(Application::class.java, *args)

    log.debug("Spring app started.")

//    logger.trace("TRACE")
//    logger.debug("DEBUG")
//    logger.info("INFO")
//    logger.warn("WARN")
//    logger.error("ERROR")
}

@SpringBootApplication(scanBasePackages = ["com.labo.kotu.*"])
class Application {

    @Autowired
    private lateinit var appSettingsService: AppSettingsService

    // XXX: not the best place for this service but it should do for now
    @Autowired
    private lateinit var sensorPollingService: SensorPollingService

    @PostConstruct
    fun init() {
        appSettingsService.startService(AppSettingsData())
        sensorPollingService.startService(Unit)
    }

    @PreDestroy
    fun onDestroy() {
        sensorPollingService.stopService()

        // TODO: DB engine shuts down before this is called so this has to happen sooner
        appSettingsService.stopService()
    }
}
