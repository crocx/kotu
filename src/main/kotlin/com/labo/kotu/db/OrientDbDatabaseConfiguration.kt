package com.labo.kotu.db

import com.labo.kotu.log
import com.orientechnologies.orient.core.db.ODatabaseType
import com.orientechnologies.orient.core.db.OrientDB
import com.orientechnologies.orient.core.db.OrientDBConfig
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.orient.`object`.OrientObjectDatabaseFactory
import org.springframework.data.orient.`object`.OrientObjectTemplate
import org.springframework.data.orient.commons.core.OrientTransactionManager
import org.springframework.data.orient.commons.repository.config.EnableOrientRepositories
import javax.annotation.PostConstruct

@Configuration
//@EnableTransactionManagement
@EnableOrientRepositories
class OrientDbDatabaseConfiguration {

    private val DB_TEST_NAME = "testDB"
    private val DB_TEST_USER = "root"
    private val DB_TEST_PASSWORD = "admin"
    private val DB_TEST_URL = "remote:localhost/$DB_TEST_NAME"

    @Bean
    fun factory(): OrientObjectDatabaseFactory {
        val orientDB = OrientDB("remote:localhost", DB_TEST_USER, DB_TEST_PASSWORD, OrientDBConfig.defaultConfig())
        val created = orientDB.createIfNotExists(DB_TEST_NAME, ODatabaseType.PLOCAL)
        if (created) {
            log.info("Created missing DB: $DB_TEST_NAME")
        }

        val factory = OrientObjectDatabaseFactory()
        factory.url = DB_TEST_URL
        factory.username = DB_TEST_USER
        factory.password = DB_TEST_PASSWORD
//        factory.maxPoolSize = 2 // XXX: this should be a check if pool instances are being acquired but not released

        log.info("Created DB factory: ${factory.url}")

        return factory
    }

    @Bean
    fun transactionManager(): OrientTransactionManager {
        log.info("Created transaction manager")
        return OrientTransactionManager(factory())
    }

    @Bean
    fun objectTemplate(): OrientObjectTemplate {
        log.info("Created object template")
        return OrientObjectTemplate(factory())
    }


    //    @Transactional
    @PostConstruct
    fun registerEntities() {
        factory().db().entityManager.registerEntityClasses("com.labo.kotu.db.data")

        factory().db().metadata.schema.generateSchema("com.labo.kotu.db.data")
        factory().db().metadata.schema.synchronizeSchema()

        factory().db().close()

        log.info("Registered class entities")
    }
}