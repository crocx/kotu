package com.labo.kotu.db.repository

import com.labo.kotu.db.data.ReportData
import org.springframework.data.orient.`object`.repository.OrientObjectRepository

interface ReportsRepository : OrientObjectRepository<ReportData>