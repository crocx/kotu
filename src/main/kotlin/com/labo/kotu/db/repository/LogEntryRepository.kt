package com.labo.kotu.db.repository

import com.labo.kotu.db.data.LogEntryData
import org.springframework.data.orient.`object`.repository.OrientObjectRepository
import org.springframework.data.orient.commons.repository.DetachMode
import org.springframework.data.orient.commons.repository.annotation.Detach
import org.springframework.data.orient.commons.repository.annotation.Query

interface LogEntryRepository : OrientObjectRepository<LogEntryData> {

    @Detach(DetachMode.ALL)
    override fun findAll(): List<LogEntryData>

    fun findByTag(tag: String): List<LogEntryData>

    @Query("select from LogEntryData where message = ?")
    fun findByMessage(message: String): List<LogEntryData>

    fun deleteByTag(tag: String): Long
}