package com.labo.kotu.db.repository

import com.labo.kotu.db.data.ProgramData
import org.springframework.data.orient.`object`.repository.OrientObjectRepository

interface ProgramDataRepository : OrientObjectRepository<ProgramData>
