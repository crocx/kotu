package com.labo.kotu.db.repository

import com.labo.kotu.db.data.AppSettingsData
import org.springframework.data.orient.`object`.repository.OrientObjectRepository
import org.springframework.data.orient.commons.repository.DetachMode
import org.springframework.data.orient.commons.repository.annotation.Detach

interface AppSettingsRepository : OrientObjectRepository<AppSettingsData> {

    @Detach(DetachMode.ALL)
    @Deprecated("There should always only be one AppSettingData so use 'findFirstBy()' instead.")
    override fun findAll(): List<AppSettingsData>

    //    @Query("select * from `AppSettingsData` limit 1")
    @Detach(DetachMode.ALL) // detach data from DB object so that it doesn't go fetch data to actual DB every time a getter is invoked (data should be cached in memory)
    fun findFirstBy(): AppSettingsData?

}