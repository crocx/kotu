package com.labo.kotu.db.data

data class LogEntryData(
        var tag: String? = null,
        var message: String = ""
) : BaseDataClass()