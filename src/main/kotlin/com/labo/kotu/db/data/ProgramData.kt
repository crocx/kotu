package com.labo.kotu.db.data

import javax.persistence.Embedded
import javax.persistence.OneToOne

class ProgramData : BaseDataClass() {

    //    @OneToOne(orphanRemoval = true)
    @Embedded
    var pidData = PidData()

    @OneToOne(orphanRemoval = true)
    var report = ReportData()

    // XXX: this type shouldn't be fixed to Double and we could have multiple setpoints
    var setpoint = 20.0

    override fun toString(): String {
        return "ProgramData(pidData=$pidData, report=$report, ${super.toString()})"
    }

}
