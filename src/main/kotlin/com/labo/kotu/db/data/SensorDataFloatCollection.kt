package com.labo.kotu.db.data

class SensorDataFloatCollection(report: ReportData, sensorId: String) : BaseSensorDataCollection<Float>(report, sensorId) {

    // has to override property from it's parent class so it can be accessed by DB
    override var data: ArrayList<Float> = arrayListOf()

}