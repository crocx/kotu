package com.labo.kotu.db.data

data class PidData(
        var kP: Double = 0.0,
        var kI: Double = 0.0,
        var kD: Double = 0.0,
        var kF: Double = 0.0
) : BaseDataClass()