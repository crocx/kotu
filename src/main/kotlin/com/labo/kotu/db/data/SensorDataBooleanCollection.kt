package com.labo.kotu.db.data

class SensorDataBooleanCollection(report: ReportData, sensorId: String) : BaseSensorDataCollection<Boolean>(report, sensorId) {

    // has to override property from it's parent class so it can be accessed by DB
    override var data: ArrayList<Boolean> = arrayListOf()

}