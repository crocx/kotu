package com.labo.kotu.db.data

import javax.persistence.Embedded

/**
 * This class shouldn't be a data class since it's copy method also copies [id] and [version] which can mismatch in DB
 * and cause the copied object not to be properly saved to DB.
 */
class AppSettingsData(var deviceId: String = "RPi",
                      var gpioPinNumber: Int = 3,

                      @Embedded
                      var pidData: PidData = PidData(),

                      var diffTolerance: Double = 0.1,
                      var refreshTime: Long = 1000, // ms
                      var sensorId: String = "temp_1"
) : BaseDataClass() {

//    @OneToOne(orphanRemoval = true)
//    var sensors = arrayListOf<Sensor>()

    constructor(data: AppSettingsData) : this() {
        copyFrom(data)
    }

    /**
     * Use this function to copy data and preserve [id] and [version] of this data object.
     */
    final fun copyFrom(data: AppSettingsData) {
        deviceId = data.deviceId
        gpioPinNumber = data.gpioPinNumber
        pidData = data.pidData
        diffTolerance = data.diffTolerance
        refreshTime = data.refreshTime
        sensorId = data.sensorId
    }

    override fun toString(): String {
        return "AppSettingsData(${super.toString()}, deviceId='$deviceId', gpioPinNumber=$gpioPinNumber, pidData=$pidData, diffTolerance=$diffTolerance, refreshTime=$refreshTime, sensorId='$sensorId')"
    }

}