package com.labo.kotu.db.data

import java.util.*

abstract class BaseSensorDataCollection<T>(
        var report: ReportData,
        var sensorId: String
) : BaseDataClass() {

    protected abstract var data: ArrayList<T>

    protected var times = arrayListOf<Date>()

    val size get() = times.size

// XXX: doesn't work with OrientDB because it thinks this is some sort of a field that needs to be handled by DB
//    operator fun get(i: Int) = times[i] to data[i]

    fun getTimeAndData(i: Int): Pair<Date, T> = times[i] to data[i]

    fun add(time: Date, data: T) {
        times.add(time)
        this.data.add(data)
    }

    fun safeAdd(time: Date, data: Any?): T? {
        val d = data as? T ?: return null
        times.add(time)
        this.data.add(d)
        return d
    }

    fun setTime(i: Int, time: Date) {
        times[i] = time
    }

    override fun toString(): String {
        return "[report = $report, sensorId = $sensorId, times = $times, data = $data, ${super.toString()}]"
    }
}