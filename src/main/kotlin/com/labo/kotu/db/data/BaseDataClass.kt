package com.labo.kotu.db.data

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.labo.kotu.main.annotation.AllOpen
import com.labo.kotu.main.annotation.NoArgsConstructor
import javax.persistence.Id
import javax.persistence.Version

@JsonIgnoreProperties(value = ["handler"])
@AllOpen
@NoArgsConstructor
abstract class BaseDataClass {

    @Id
    var id: Any? = null
        protected set

    @Version
    @JsonIgnore
    var version: Any? = null
        protected set

    override fun toString(): String {
        return "[id = $id, version = $version]"
    }

}