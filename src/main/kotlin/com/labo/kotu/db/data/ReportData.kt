package com.labo.kotu.db.data

import com.labo.kotu.tool.Util
import java.util.*
import javax.persistence.OneToOne

// TODO: add a list of setpoints and their timestamps
data class ReportData(
        var date: Date = Util.currentTime,
        var notes: String? = null,
        var loggingInterval: Long = 1000 * 60 * 1 // every minute - in ms
) : BaseDataClass() {

    @OneToOne(orphanRemoval = true) // needed to remove all the linked data if this parent gets removed from DB
    var sensorsData: ArrayList<BaseSensorDataCollection<*>> = arrayListOf()
}
