package com.labo.kotu.main.service

import kotlin.concurrent.thread

abstract class BaseThreadService<T : Any> : BaseSuspendableService<T>() {

    protected abstract var tagServiceThread: String

    // TODO: higher order function could calculate this timeout given some wanted "refresh/loop time"
    /** @return the number of milliseconds before the next loop iteration */
    protected abstract fun inServiceLoop(): Long

    private lateinit var serviceThread: Thread

    private var wantedStatus = Status.NOT_STARTED

    protected fun isServiceThreadAlive() = ::serviceThread.isInitialized && serviceThread.isAlive

    protected fun wantToRun() = wantedStatus == Status.RUNNING
    protected fun wantToPause() = wantedStatus == Status.PAUSED

    protected open fun onServiceLoopStart() {} // can be overridden if needed
    protected open fun onServiceLoopPause() {} // can be overridden if needed
    protected open fun onServiceLoopResume() {} // can be overridden if needed
    protected open fun onServiceLoopEnd() {} // can be overridden if needed
    // can be overridden if needed
    /** @return **true** to continue the service loop, **false** (default) to stop it. */
    protected open fun onServiceLoopException(e: Exception): Boolean {
        return false
    }

    protected open fun serviceThreadRunnable() {
        onServiceLoopStart()

        while (wantToRun()) {
            try {
                val timeout = inServiceLoop()
                Thread.sleep(timeout)
            } catch (e: InterruptedException) {
                if (!wantToPause()) {
                    log.info("Loop interrupted but not set to pause it - exiting loop", tagServiceThread)
                    break
                }

                onServiceLoopPause()

                if (!checkAndPauseLoop()) break

                onServiceLoopResume()
            } catch (ex: Exception) {
                log.error("Loop exception: ${ex.message}", tagServiceThread)

                if (!onServiceLoopException(ex)) break
            }
        }

        onServiceLoopEnd()
    }

    override fun start(): Boolean {
        if (isServiceThreadAlive()) {
            log.warn("Can't start service thread - already alive", tagServiceThread)
            return false
        }

        wantedStatus = Status.RUNNING
        log.info("Starting service thread...")

        serviceThread = thread(name = className) {
            status = Status.RUNNING
            log.info("Service thread started", tagServiceThread)

            serviceThreadRunnable()

            log.info("Service thread ended", tagServiceThread)
            status = Status.ENDED
        }

        waitUntilStatus(Status.RUNNING)
        return true
    }

    override fun pause(): Boolean {
        if (!isServiceThreadAlive()) {
            log.warn("Can't pause service thread - not alive", tagServiceThread)
            return false
        }

        wantedStatus = Status.PAUSED
        serviceThread.interrupt()
        waitUntilStatus(Status.PAUSED)
        return isPaused()
    }

    override fun resume(): Boolean {
        if (!isServiceThreadAlive()) {
            log.warn("Can't resume service thread - not alive", tagServiceThread)
            return false
        }

        wantedStatus = Status.RUNNING
        serviceThread.interrupt()
        waitUntilStatus(Status.RUNNING)
        return isRunning()
    }

    override fun stop(): Boolean {
        if (!isServiceThreadAlive()) {
            log.warn("Can't stop service thread - not alive", tagServiceThread)
            return false
        }

        wantedStatus = Status.ENDED
        serviceThread.interrupt()
        serviceThread.join()
        waitUntilStatus(Status.ENDED)
        return isStopped()
    }

    /**
     * @return **true** to continue, **false** to break from the loop
     */
    protected fun checkAndPauseLoop(): Boolean {
        log.info("Loop interrupted: wantedStatus = $wantedStatus", tagServiceThread)
        if (wantedStatus != Status.PAUSED) return false

        try {
            status = Status.PAUSED
            while (wantedStatus == Status.PAUSED) Thread.sleep(10000)
        } catch (e2: InterruptedException) {
            log.info("Loop interrupted while paused: wantedStatus = $wantedStatus", tagServiceThread)
            if (wantedStatus != Status.RUNNING) return false
        }

        status = Status.RUNNING
        return true
    }

}