package com.labo.kotu.main.service

import com.labo.kotu.db.data.ReportData
import com.labo.kotu.hw.sensor.PollableSensor
import com.labo.kotu.hw.sensor.Sensors
import com.labo.kotu.hw.service.SensorPollingService
import com.labo.kotu.tool.Util
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ReportLoggingService : BaseThreadService<ReportData>() {

    override var tagServiceThread = "RLS"

    private val sensorPollTime = 1000L // XXX: this shouldn't be a constant but something per-sensor
    private val pollingSensors = hashMapOf<String, PollableSensor<*>>()

    @Autowired
    private lateinit var sensors: Sensors
    @Autowired
    private lateinit var sensorsPollingService: SensorPollingService

    override fun onServiceLoopStart() {
        data.date = Util.currentTime
    }

    override fun inServiceLoop(): Long {
        val time = Util.currentTime
        for (dataCollection in data.sensorsData) {
            val sensor = pollingSensors[dataCollection.sensorId]
            if (sensor == null) {
                log.warn("Can't find sensor: ${dataCollection.sensorId}", tagServiceThread)
                continue
            }

            val sensorData = sensorsPollingService.getData(sensor)
            if (sensorData == null) {
                log.warn("Can't get data for sensor: ${sensor.sensorId}", tagServiceThread)
                continue
            }

            dataCollection.safeAdd(time, sensorData.data)?.let { log.debug("Added data: $it on $time") }
                    ?: log.warn("Can't add data for sensor: ${sensor.sensorId}, data = $sensorData", tagServiceThread)
            // TODO: save the added item to the DB
        }

        return Math.max(0, data.loggingInterval - (Util.currentTime.time - time.time)) // timeout should be positive
    }

    override fun start(): Boolean {
        for (dataCollection in data.sensorsData) {
            val sensor = sensors[dataCollection.sensorId] as? PollableSensor<*>
            if (sensor == null) {
                log.warn("Couldn't find sensor '${dataCollection.sensorId}' to start polling.")
                return false
            }

            if (!sensorsPollingService.startSensorPolling(sensor, sensorPollTime)) {
                log.warn("Couldn't start polling sensor: '$sensor'")
                return false
            }

            pollingSensors[sensor.sensorId] = sensor
        }

        return super.start()
    }

}
