package com.labo.kotu.main.service

import com.labo.kotu.tool.LLog
import javax.annotation.PostConstruct

abstract class BaseService<T : Any> {

    enum class Status {
        NOT_STARTED, RUNNING, PAUSED, ENDED
    }

    protected abstract fun start(): Boolean
    protected abstract fun stop(): Boolean

    val className by lazy { this::class.simpleName!! }

    lateinit var data: T
        protected set

    var status = Status.NOT_STARTED
        protected set

    protected val log = LLog

    override fun toString() = "$className(status=$status, data=${if (::data.isInitialized) data.toString() else "<NOT YET INITIALIZED>"})"

    fun isRunning() = status == Status.RUNNING
    fun isStopped() = status == Status.ENDED

    fun logServiceStatus() = log.debug("status = $status", className)

    @PostConstruct
    open fun postConstruct() {
        // can be overridden if needed
    }

    /**
     * Polling until given waitForStatus is set.
     */
    fun waitUntilStatus(waitForStatus: Status) {
        while (status != waitForStatus) Thread.yield()
    }

    fun startService(data: T): Boolean {
        log.debug("Starting service: $className, with data: $data")
        if (status == Status.RUNNING) {
            log.warn("Can't start $className - already started")
            return false
        }

        this.data = data
        if (start()) status = Status.RUNNING
        log.debug("$className status = $status")
        return isRunning()
    }

    fun stopService(): Boolean {
        log.debug("Stopping service: $className")
        if (status == Status.NOT_STARTED || status == Status.ENDED) {
            log.warn("Can't stop $className - not running: status = $status")
            return false
        }

        if (stop()) status = Status.ENDED
        log.debug("$className status = $status")
        return isStopped()
    }

}