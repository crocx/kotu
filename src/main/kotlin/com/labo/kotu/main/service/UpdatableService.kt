package com.labo.kotu.main.service

interface UpdatableService {

    fun updateService(): Boolean
}