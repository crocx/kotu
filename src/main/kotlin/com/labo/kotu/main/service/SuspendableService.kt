package com.labo.kotu.main.service

interface SuspendableService {

    fun pause(): Boolean
    fun resume(): Boolean

    fun isPaused(): Boolean
}