package com.labo.kotu.main.service

abstract class BaseSuspendableService<T : Any> : BaseService<T>(), SuspendableService, UpdatableService {

    override fun isPaused() = status == Status.PAUSED

    fun isRunningOrPaused() = isRunning() || status == Status.PAUSED

    fun pauseService(): Boolean {
        log.debug("Pausing service: $className")
        if (!isRunning()) {
            log.warn("Can't pause $className - not running")
            return false
        }

        if (pause()) status = Status.PAUSED
        log.debug("$className status = $status")
        return isPaused()
    }

    fun resumeService(): Boolean {
        log.debug("Resuming service: $className")
        if (!isPaused()) {
            log.warn("Can't resume $className - not paused")
            return false
        }

        if (resume()) status = Status.RUNNING
        log.debug("$className status = $status")
        return isRunning()
    }

    protected open fun update(): Boolean {
        return true // nothing special to update, can be overridden if necessary
    }

    override fun updateService(): Boolean {
        log.debug("Updating service: $className, status = $status")
        if (!isRunningOrPaused()) {
            log.warn("Can't update $className - not running")
            return false
        }

        return update().also { log.debug("$className updated = $it") }
    }

}