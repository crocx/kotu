package com.labo.kotu.main.service

import com.labo.kotu.db.OrientDbDatabaseConfiguration
import com.labo.kotu.db.data.ProgramData
import com.labo.kotu.db.repository.ProgramDataRepository
import com.labo.kotu.hw.sensor.Sensors
import com.labo.kotu.main.data.TemperatureControlData
import com.labo.kotu.tool.isFalse
import com.orientechnologies.orient.`object`.db.OObjectDatabaseTx
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProgramService : BaseSuspendableService<ProgramData>() {

    @Autowired
    private lateinit var temperatureControlService: TemperatureControlService
    @Autowired
    private lateinit var reportLoggingService: ReportLoggingService
    @Autowired
    private lateinit var sensors: Sensors
    @Autowired
    private lateinit var programDataRepository: ProgramDataRepository
    @Autowired
    private lateinit var dbConfig: OrientDbDatabaseConfiguration

    private val services = arrayListOf<BaseSuspendableService<*>>()
    private lateinit var db: OObjectDatabaseTx

    override fun postConstruct() {
        super.postConstruct()
        db = dbConfig.factory().db()
    }

    override fun start(): Boolean {
        data.report.sensorsData = sensors.generateDataCollection(data.report)

        log.info("Starting services...")
        // XXX: could be done nicer but there are some problems with generics
        startServiceWithData(temperatureControlService, TemperatureControlData(sensors.getMainSensorByType(), data.setpoint)).isFalse { return false }
        startServiceWithData(reportLoggingService, data.report).isFalse { return false }
        log.info("${services.size} services started")

        // XXX: if saving program crashes/freezes services should be stopped
        saveProgram()
        return true
    }

    override fun updateService(): Boolean {
        updateServices().isFalse { return false }
        saveProgram()
        return true
    }

    override fun pause(): Boolean {
        services.forEach { it.pauseService().isFalse { return false } }
        saveProgram()
        return true
    }

    override fun resume(): Boolean {
        services.forEach { it.resumeService().isFalse { return false } }
        saveProgram()
        return true
    }

    override fun stop(): Boolean {
        // can't use foreach here because we're removing successfully stopped services from the list
        while (services.isNotEmpty()) {
            services.first().stopService().isFalse { return false }
            services.removeAt(0)
        }
        saveProgram()
        return true
    }

    private fun saveProgram() {
        if (!db.isActiveOnCurrentThread) db.activateOnCurrentThread()
        log.debug("Saving program to DB: $data")
        data = programDataRepository.save(data)
        data = db.detach(data, true)
        log.debug("Program saved: $data")
        db.close() // have to call this after accessing DB objects otherwise we run out of pool connections
    }

    private fun <T : Any> startServiceWithData(service: BaseSuspendableService<T>, data: T): Boolean {
        service.startService(data).isFalse { return false }
        services.add(service)
        return true
    }

    private fun updateServices(): Boolean {
        services.forEach { it.updateService().isFalse { return false } }
        return true
    }

}
