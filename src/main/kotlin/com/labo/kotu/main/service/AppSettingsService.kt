package com.labo.kotu.main.service

import com.labo.kotu.db.OrientDbDatabaseConfiguration
import com.labo.kotu.db.data.AppSettingsData
import com.labo.kotu.db.repository.AppSettingsRepository
import com.labo.kotu.main.AppSettings
import com.orientechnologies.orient.`object`.db.OObjectDatabaseTx
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import kotlin.concurrent.thread

@Service
class AppSettingsService : BaseService<AppSettingsData>(), UpdatableService {

    @Autowired
    private lateinit var appSettingsRepository: AppSettingsRepository
    @Autowired
    private lateinit var dbConfig: OrientDbDatabaseConfiguration

    var saveTimeout = 2000L // ms
        protected set

    private var saveThread: Thread? = null
    private lateinit var db: OObjectDatabaseTx

    override fun postConstruct() {
        super.postConstruct()
        db = dbConfig.factory().db()
    }

    override fun start(): Boolean {
        appSettingsRepository.findFirstBy()?.let { data = it }
                ?: let {
                    log.warn("No prior app settings found, using new one")
                    saveSettingsNow()
                }
        AppSettings.init(this)

        // detach data from DB object so that it doesn't go fetch data to actual DB every time a getter is invoked (data should be cached in memory)
//        data = db.detachAll(data, true)
        log.debug("Loaded app settings from DB: $data")
        return true
    }

    override fun stop(): Boolean {
        saveSettingsNow()
        return true
    }

    override fun updateService(): Boolean {
//        saveSettings()
        saveSettingsNow() // XXX: is this OK
        return true
    }

    fun saveSettings() {
        postSave()
    }

    fun saveSettingsNow(): AppSettingsData {
        log.debug("Saving app data to DB:\n\t$data")
        if (db.isClosed) db = dbConfig.factory().openDatabase()
        if (!db.isActiveOnCurrentThread) {
            log.debug("Activating DB on current thread")
            db.activateOnCurrentThread()
        }
        data = appSettingsRepository.save(data)
        // detach data from DB object so that it doesn't go fetch data to actual DB every time a getter is invoked (data should be cached in memory)
        data = db.detachAll(data, true)
        log.debug("Saved data:\n\t$data")
        db.close() // have to call this after accessing DB objects otherwise we run out of pool connections
        return data
    }

    private fun postSave() {
        saveThread?.let { log.debug("Save thread already pending...") }
                ?: thread {
                    log.debug("Will save to DB in $saveTimeout ms")
                    Thread.sleep(saveTimeout)
                    saveSettingsNow()
                    saveThread = null
                }
    }

    fun copyData(newData: AppSettingsData) {
        log.info("Copying data:\nold data: ${this.data}\nnew data: $data")
        data.copyFrom(newData)
        saveSettingsNow()
    }
}