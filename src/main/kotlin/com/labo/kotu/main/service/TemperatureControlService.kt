package com.labo.kotu.main.service

import com.labo.kotu.hw.PinController
import com.labo.kotu.hw.controller.SensorPollingController
import com.labo.kotu.hw.data.FloatSensorData
import com.labo.kotu.hw.data.SensorErrorCode
import com.labo.kotu.hw.sensor.Sensors
import com.labo.kotu.hw.sensor.TemperatureSensor
import com.labo.kotu.main.AppSettings
import com.labo.kotu.main.data.TemperatureControlData
import com.labo.kotu.tool.MiniPID
import com.labo.kotu.tool.isFalse
import com.pi4j.io.gpio.PinState
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TemperatureControlService : BaseThreadService<TemperatureControlData>() {

    override var tagServiceThread = "TSC"

    var isInTestModeOverride: Boolean? = null

    private val isInTestMode
        get() = isInTestModeOverride ?: sensors.getMainSensorByType<TemperatureSensor>().isGettingTestData

    @Autowired
    private lateinit var pinController: PinController
    @Autowired
    private lateinit var pollingController: SensorPollingController
    @Autowired
    private lateinit var sensors: Sensors

    private var pid: MiniPID = MiniPID(1.0, 1.0, 1.0)
    private var previousCorrection = 0.0

    private fun pollTemperature(temperatureSensor: TemperatureSensor) = pollingController.getSensorData(temperatureSensor) as? FloatSensorData

    fun getTemperature(sensor: TemperatureSensor): FloatSensorData = pollTemperature(sensor)
            ?: FloatSensorData(sensor.sensorId, SensorErrorCode.SENSOR_NOT_POLLING)

    override fun inServiceLoop(): Long {
        val temperature = pollTemperature(data.sensor)?.data ?: let {
            log.warn("No temperature data for sensor: ${data.sensor}")
            return AppSettings.data.refreshTime
        }

        val correction = pid.getOutput(temperature.toDouble())
        val diffCorrection = correction - previousCorrection
        previousCorrection = correction

        val turnOn = diffCorrection > AppSettings.data.diffTolerance
        log.debug("PID: corr = $correction, diffCorr = $diffCorrection, temp = $temperature, turnOn = $turnOn", tagServiceThread)

        pinController.turnOn(turnOn)
        return AppSettings.data.refreshTime
    }

    override fun onServiceLoopPause() {
        pinController.turnOn(false) // turn of for safety
    }

    override fun onServiceLoopEnd() {
        pinController.unprovisionPin()
    }

    // TODO: would be great if these BaseResponseCodes could somehow be returned
    override fun start(): Boolean {
        // if not yet polling sensor but also can't start polling it, return false
        if (!pollingController.isAlreadyPollingSensor(data.sensor) && !pollingController.startSensorPolling(data.sensor)) {
//            return createServiceResponse("Cannot start polling sensor: $sensor", BaseResponseCodes.ERROR_CANNOT_POLL_SENSOR)
            log.error("Cannot start polling sensor: ${data.sensor}")
            return false
        }

        update().isFalse { return false }

        try {
            // Relay is turned ON with low pin state and OFF with high pin state.
            val pinProvisioned = pinController.provisionPin(AppSettings.data.gpioPinNumber, PinState.HIGH, isInTestMode)
            if (!pinProvisioned) {
//                return createServiceResponse("Could not provision pin: $gpioPinNumber, isInTestMode = $isInTestMode",
//                        BaseResponseCodes.ERROR_HW_PIN_PROVISIONING_FAILED)
                log.error("Could not provision pin: ${AppSettings.data.gpioPinNumber}, isInTestMode = $isInTestMode")
                return false
            }
        } catch (e: Exception) {
//            return createServiceResponse("Exception provisioning pin: $gpioPinNumber, isInTestMode = $isInTestMode, exception = ${e.message}",
//                    BaseResponseCodes.ERROR_HW_PIN_PROVISIONING_FAILED)
            log.error("Exception provisioning pin: ${AppSettings.data.gpioPinNumber}, isInTestMode = $isInTestMode, exception = ${e.message}")
            return false
        }

        return super.start()
    }

    override fun update(): Boolean {
        pid.setSetpoint(data.setpoint)
        pid.reset()
        previousCorrection = 0.0

        return true
    }

}