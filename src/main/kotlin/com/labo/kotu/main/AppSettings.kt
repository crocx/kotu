package com.labo.kotu.main

import com.labo.kotu.db.data.AppSettingsData
import com.labo.kotu.main.service.AppSettingsService

/**
 * A "shortcut" for using AppSettingsService (no need to @Autowire AppSettingsServices everywhere)
 */
object AppSettings {

    val data: AppSettingsData get() = service.data

    lateinit var service: AppSettingsService
        private set

    fun init(service: AppSettingsService) {
        this.service = service
    }

    fun saveSettings() {
        service.saveSettings()
    }

    fun saveSettingsNow() {
        service.saveSettingsNow()
    }
}
