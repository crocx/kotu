package com.labo.kotu.main.annotation

@Target(AnnotationTarget.CLASS)
annotation class AllOpen