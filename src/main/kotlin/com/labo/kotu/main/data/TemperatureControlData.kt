package com.labo.kotu.main.data

import com.labo.kotu.hw.sensor.TemperatureSensor

data class TemperatureControlData(var sensor: TemperatureSensor, var setpoint: Double)
