package com.labo.kotu.tool

import java.util.*

object Util {

    val currentTime: Date get() = Date()
    val random = Random()

    fun nextDouble(lowerLimit: Double = 0.0, upperLimit: Double = 1.0) = random.nextDouble() * (upperLimit - lowerLimit) - lowerLimit

    fun nextFloat(lowerLimit: Float = 0.0f, upperLimit: Float = 1.0f) = random.nextFloat() * (upperLimit - lowerLimit) - lowerLimit
}

operator fun Date.plus(add: Int) = Date(time + add)
operator fun Date.plus(add: Long) = Date(time + add)

fun Date.addSeconds(add: Int) = Date(time + add * 1000)

/** Call the specified function [block] if this boolean value is true. */
inline fun Boolean.isTrue(block: () -> Unit): Unit = if (this) block() else Unit

/** Call the specified function [block] if this boolean value is false. */
inline fun Boolean.isFalse(block: () -> Unit): Unit = if (!this) block() else Unit
