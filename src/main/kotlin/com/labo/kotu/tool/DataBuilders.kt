package com.labo.kotu.tool

import com.labo.kotu.db.data.ProgramData
import com.labo.kotu.main.AppSettings
import com.labo.kotu.web.data.AppSettingsWebData

// use this singleton if you can't extend data class with generating an instance of it first
object DataBuilders {
}

fun ProgramData.builder(setpoint: Double = 20.0, copyAppSettingsData: Boolean = true, autoGenerateNotes: Boolean = true): ProgramData {
    this.setpoint = setpoint

    if (copyAppSettingsData) pidData = AppSettings.data.pidData
    if (autoGenerateNotes) report.notes = "Program started on: ${report.date} @ ${setpoint}°C"
    return this
}

fun AppSettingsWebData.build(): AppSettingsWebData {
    sensorId = AppSettings.data.sensorId
    diffTolerance = AppSettings.data.diffTolerance
    refreshTime = AppSettings.data.refreshTime
    gpio = AppSettings.data.gpioPinNumber
    kP = AppSettings.data.pidData.kP
    kI = AppSettings.data.pidData.kI
    kD = AppSettings.data.pidData.kD
    return this
}
