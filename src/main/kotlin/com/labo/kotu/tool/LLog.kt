package com.labo.kotu.tool

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.slf4j.event.Level

//@Component
/**
 * Singleton (custom) logger.
 */
object LLog {

    private val MDC_TAG_CALLER_CLASS = "callerClass"
    private val MDC_TAG_CALLER_METHOD = "callerMethod"
    private val MDC_TAG_TAG = "tag"

    private val logger: Logger = LoggerFactory.getLogger(LLog::class.java)

    fun trace(message: String, tag: String? = null) {
        log(Level.TRACE, tag, message)
    }

    fun debug(message: String, tag: String? = null) {
        log(Level.DEBUG, tag, message)
    }

    fun info(message: String, tag: String? = null) {
        log(Level.INFO, tag, message)
    }

    fun warn(message: String, tag: String? = null) {
        log(Level.WARN, tag, message)
    }

    fun error(message: String, tag: String? = null) {
        log(Level.ERROR, tag, message)
    }

    fun log(level: Level, tag: String? = null, message: String, traceDepth: Int = 4) {
        // XXX: performance hit - consider removing it in some cases (production...)
        var stack = Throwable().stackTrace[traceDepth]

        MDC.put(MDC_TAG_CALLER_CLASS, "${stack.fileName}:${stack.lineNumber}")
        MDC.put(MDC_TAG_CALLER_METHOD, stack.methodName)
        if (tag != null) MDC.put(MDC_TAG_TAG, " #$tag:")

        when (level) {
            Level.TRACE -> logger.trace(message)
            Level.DEBUG -> logger.debug(message)
            Level.INFO -> logger.info(message)
            Level.WARN -> logger.warn(message)
            Level.ERROR -> logger.error(message)
        }

        MDC.clear()
    }

}