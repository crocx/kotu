# kotu

This is a **Kotlin (1.2)** project.

## How to set up the project and run it

1. Make sure you have Java 1.8 installed.
2. Clone the project via:

   `git clone https://github.com/crocy/kotu.git`

3. Go to the project folder and run `./gradlew bootRun` (or just `gradlew bootRun` on Windows) for testing on local
   machine. To run the app on your RPi, root privileges are needed to access RPi's GPIOs, so you'll need to start the app
   with `sudo ./gradlew bootRun`
   
   1. If you get a `./gradlew: command not found` error, you might need to set proper permissions on your Unix
      machine via `chmod +x gradlew`

4. Gradle might download all the needed dependencies and recompile your source code (if it has never done that yet or if
   there are changes to it) and then start Spring Boot with the embedded Tomcat server running this app on port 3001 by
   default. You should see something like this:
   ```
    3:18:59.242 [main] DEBUG com.kotu.Main - Starting Spring app...
    13:19:00.036 [main] DEBUG org.springframework.boot.devtools.settings.DevToolsSettings - Included patterns for restart : []
    13:19:00.036 [main] DEBUG org.springframework.boot.devtools.settings.DevToolsSettings - Excluded patterns for restart : [/spring-boot-starter/target/classes/, /spring-boot-autoconfigure/target/classes/, /spring-boot-starter-[\w-]+/, /spring-boot/target/classes/, /spring-boot-actuator/target/classes/, /spring-boot-devtools/target/classes/]
    13:19:00.040 [main] DEBUG org.springframework.boot.devtools.restart.ChangeableUrls - Matching URLs for reloading : [file:/home/pi/work/git/kotu/src/main/resources/, file:/home/pi/work/git/kotu/build/classes/java/main/, file:/home/pi/work/git/kotu/build/resources/main/]
    13:19:00.068 [restartedMain] DEBUG com.kotu.Main - Starting Spring app...
    
      .   ____          _            __ _ _
     /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
    ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
     \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
      '  |____| .__|_| |_|_| |_\__, | / / / /
     =========|_|==============|___/=/_/_/_/
     :: Spring Boot ::        (v1.5.9.RELEASE)
    
    2018-01-12 13:19:01.447  INFO 24604 --- [  restartedMain] com.kotu.Main                         : Starting Main on raspberrypi with PID 24604 (/home/pi/work/git/kotu/build/classes/java/main started by root in /home/pi/work/git/kotu)
    2018-01-12 13:19:01.462 DEBUG 24604 --- [  restartedMain] com.kotu.Main                         : Running with Spring Boot v1.5.9.RELEASE, Spring v4.3.13.RELEASE
    2018-01-12 13:19:01.463  INFO 24604 --- [  restartedMain] com.kotu.Main                         : No active profile set, falling back to default profiles: default
    2018-01-12 13:19:01.763  INFO 24604 --- [  restartedMain] ationConfigEmbeddedWebApplicationContext : Refreshing org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@5ff97e: startup date [Fri Jan 12 13:19:01 CET 2018]; root of context hierarchy
    2018-01-12 13:19:08.434  INFO 24604 --- [  restartedMain] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat initialized with port(s): 3001 (http)
    2018-01-12 13:19:08.488  INFO 24604 --- [  restartedMain] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
    2018-01-12 13:19:08.493  INFO 24604 --- [  restartedMain] org.apache.catalina.core.StandardEngine  : Starting Servlet Engine: Apache Tomcat/8.5.23
    2018-01-12 13:19:08.854  INFO 24604 --- [ost-startStop-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
    2018-01-12 13:19:08.856  INFO 24604 --- [ost-startStop-1] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 7107 ms
    2018-01-12 13:19:09.527  INFO 24604 --- [ost-startStop-1] o.s.b.w.servlet.ServletRegistrationBean  : Mapping servlet: 'dispatcherServlet' to [/]
    2018-01-12 13:19:09.543  INFO 24604 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'characterEncodingFilter' to: [/*]
    2018-01-12 13:19:09.546  INFO 24604 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'hiddenHttpMethodFilter' to: [/*]
    2018-01-12 13:19:09.547  INFO 24604 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'httpPutFormContentFilter' to: [/*]
    2018-01-12 13:19:09.547  INFO 24604 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'requestContextFilter' to: [/*]
    2018-01-12 13:19:11.159  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerAdapter : Looking for @ControllerAdvice: org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@5ff97e: startup date [Fri Jan 12 13:19:01 CET 2018]; root of context hierarchy
    2018-01-12 13:19:11.503  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/api/v1/temp || /api/v1/temp/{id}],methods=[GET]}" onto public com.kotu.data.TemperatureData com.kotu.service.TemperatureService.getTemperature(java.util.Optional<java.lang.String>)
    2018-01-12 13:19:11.515  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/api/v1/temp/start/{id}/{setpoint}],methods=[GET]}" onto public com.kotu.data.MessageData com.kotu.service.TemperatureService.startControlService(java.lang.String,float,int,float,float,float,float,long)
    2018-01-12 13:19:11.518  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/api/v1/temp/update/{id}/{setpoint}],methods=[GET]}" onto public com.kotu.data.MessageData com.kotu.service.TemperatureService.updateControlService(java.lang.String,float,int,float,float,float,float,long)
    2018-01-12 13:19:11.524  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/api/v1/temp/stop || /api/v1/temp/stop/{id}],methods=[GET]}" onto public com.kotu.data.MessageData com.kotu.service.TemperatureService.stopControlService(java.util.Optional<java.lang.String>)
    2018-01-12 13:19:11.541  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error],produces=[text/html]}" onto public org.springframework.web.servlet.ModelAndView org.springframework.boot.autoconfigure.web.BasicErrorController.errorHtml(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)
    2018-01-12 13:19:11.543  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error]}" onto public org.springframework.http.ResponseEntity<java.util.Map<java.lang.String, java.lang.Object>> org.springframework.boot.autoconfigure.web.BasicErrorController.error(javax.servlet.http.HttpServletRequest)
    2018-01-12 13:19:11.705  INFO 24604 --- [  restartedMain] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/webjars/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
    2018-01-12 13:19:11.706  INFO 24604 --- [  restartedMain] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
    2018-01-12 13:19:11.922  INFO 24604 --- [  restartedMain] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**/favicon.ico] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
    2018-01-12 13:19:12.587  INFO 24604 --- [  restartedMain] o.s.b.d.a.OptionalLiveReloadServer       : LiveReload server is running on port 35729
    2018-01-12 13:19:12.890  INFO 24604 --- [  restartedMain] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
    2018-01-12 13:19:13.112  INFO 24604 --- [  restartedMain] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port(s): 3001 (http)
    2018-01-12 13:19:13.133  INFO 24604 --- [  restartedMain] com.kotu.Main                         : Started Main in 13.039 seconds (JVM running for 14.613)
    2018-01-12 13:19:13.134 DEBUG 24604 --- [  restartedMain] com.kotu.Main                         : Spring app started.
    2018-01-12 13:19:17.412  INFO 24604 --- [nio-3001-exec-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring FrameworkServlet 'dispatcherServlet'
    2018-01-12 13:19:17.412  INFO 24604 --- [nio-3001-exec-1] o.s.web.servlet.DispatcherServlet        : FrameworkServlet 'dispatcherServlet': initialization started
    2018-01-12 13:19:17.502  INFO 24604 --- [nio-3001-exec-1] o.s.web.servlet.DispatcherServlet        : FrameworkServlet 'dispatcherServlet': initialization completed in 89 ms
   ```

   In that output the lines below should tell you what API calls are implemented/supported:

   ```
    2018-01-12 13:19:11.503  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/api/v1/temp || /api/v1/temp/{id}],methods=[GET]}" onto public com.kotu.data.TemperatureData com.kotu.service.TemperatureService.getTemperature(java.util.Optional<java.lang.String>)
    2018-01-12 13:19:11.515  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/api/v1/temp/start/{id}/{setpoint}],methods=[GET]}" onto public com.kotu.data.MessageData com.kotu.service.TemperatureService.startControlService(java.lang.String,float,int,float,float,float,float,long)
    2018-01-12 13:19:11.518  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/api/v1/temp/update/{id}/{setpoint}],methods=[GET]}" onto public com.kotu.data.MessageData com.kotu.service.TemperatureService.updateControlService(java.lang.String,float,int,float,float,float,float,long)
    2018-01-12 13:19:11.524  INFO 24604 --- [  restartedMain] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/api/v1/temp/stop || /api/v1/temp/stop/{id}],methods=[GET]}" onto public com.kotu.data.MessageData com.kotu.service.TemperatureService.stopControlService(java.util.Optional<java.lang.String>)
   ```
   
   1. Once Spring Boot is up and running you can deploy any app changes just by building a new .jar via `./gradlew build`
      
      Spring Boot should detect file changes and reload the server with a new .jar. If you think any changes aren't
      included in the new run it's possible you'll need to restart Spring Boot (just kill the process via `Ctrl + C` and
      restart with `./gradlew bootRun` or `sudo ./gradlew bootRun` if running or RPi).
   
5. If you're running the [Ango](https://github.com/crocy/ango) web interface project as well, you'll just need to know the IP address of your RPi and
   connect to it's `3001` port to use that service via web UI.